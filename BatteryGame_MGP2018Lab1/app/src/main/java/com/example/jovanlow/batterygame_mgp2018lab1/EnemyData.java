package com.example.jovanlow.batterygame_mgp2018lab1;

public class EnemyData {
    public enum SpawnPoint
    {
        TOPLEFT,
        TOPRIGHT,
        BOTTOMLEFT,
        BOTTOMRIGHT,
    }

    SampleEntity.Type enemytype = SampleEntity.Type.FossilCar;
    double timeToSpawn = 0;
    SpawnPoint spawn = SpawnPoint.TOPLEFT;
    boolean canSpecial = false;
    float hp = 1;
    float damage = 1;

    public EnemyData (SampleEntity.Type enemytype_, double timeToSpawn_, SpawnPoint spawn_, boolean canSpecial_, float hp_, float damage_)
    {
        enemytype = enemytype_;
        timeToSpawn = timeToSpawn_;
        spawn = spawn_;
        canSpecial = canSpecial_;
        hp = hp_;
        damage = damage_;
    }

}
