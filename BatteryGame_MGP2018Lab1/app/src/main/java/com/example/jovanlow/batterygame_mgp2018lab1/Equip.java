package com.example.jovanlow.batterygame_mgp2018lab1;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

import android.widget.ImageView;
import android.widget.TextView;
import java.io.InputStreamReader;
import java.io.InputStream;
import android.content.res.Resources;
import java.io.BufferedOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;


public class Equip extends Activity implements OnClickListener {

    //Define an object and pass it to another method to use
    private Button btn_menu, btn_car_left, btn_car_right, btn_wep_left, btn_wep_right;

    //private OutputStream outputStream = res.openRawResource(R.raw.dead);
    private String line;
    private int Loot;
    private TextView tv;

    private ImageView vehicleImg;
    private ImageView weaponImg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //code for hiding UI
        requestWindowFeature(Window.FEATURE_NO_TITLE); //hide title
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN); //hide top bar

        setContentView(R.layout.equip);

//        Resources res = getResources();
//        InputStream inputStream = res.openRawResource(R.raw.dead);
//
//        //read from file here
//        StringBuilder text = new StringBuilder();


//        try {
//            BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
//            while ((line = br.readLine()) != null) {
//                text.append(line);
//                text.append('\n');
//                Loot = Integer.valueOf(line);
//            }
//            br.close() ;
//        }catch (IOException e) {
//            e.printStackTrace();
//        }
//

        //tv = (TextView)findViewById(R.id.amount);
        //tv.setText(text.toString()); ////Set the text to text view.

        //when leaving equip write to file

        //Set listener to button
        btn_menu = (Button) findViewById(R.id.btn_menu);
        btn_menu.setOnClickListener(this);

        btn_car_left = (Button) findViewById(R.id.btn_car_left);
        btn_car_left.setOnClickListener(this);
        btn_car_right = (Button) findViewById(R.id.btn_car_right);
        btn_car_right.setOnClickListener(this);
        btn_wep_left = (Button) findViewById(R.id.btn_wep_left);
        btn_wep_left.setOnClickListener(this);
        btn_wep_right = (Button) findViewById(R.id.btn_wep_right);
        btn_wep_right.setOnClickListener(this);

        vehicleImg = (ImageView)findViewById(R.id.vehicleimg);
        if (MusicController.Instance.cartype.compareTo("Electric Car") == 0)
            vehicleImg.setImageResource(R.drawable.bw_car);
        else if (MusicController.Instance.cartype.compareTo("Electric Bus") == 0)
            vehicleImg.setImageResource(R.drawable.bw_bus);

        weaponImg = (ImageView)findViewById(R.id.weaponimg);
        if (MusicController.Instance.CurrentWeapon == SampleEntity.ProjType.PlayerProj_FutureGhost)
            weaponImg.setImageResource(R.drawable.bw_proj);
        else if (MusicController.Instance.CurrentWeapon == SampleEntity.ProjType.PlayerProj_Boomerang)
            weaponImg.setImageResource(R.drawable.bw_proj_boomerang);
        else if (MusicController.Instance.CurrentWeapon == SampleEntity.ProjType.PlayerProj_JumpingJack)
            weaponImg.setImageResource(R.drawable.bw_proj_boomerang); //change this

//        increaseNum = (Button) findViewById(R.id.increase);
//        increaseNum.setOnClickListener(this);
//
//        decreaseNum = (Button) findViewById(R.id.decrease);
//        decreaseNum.setOnClickListener(this);

    }

    @Override // will happen if there is an tap of the button on the screen/view
    public void onClick(View v)
    {
        Intent intent = new Intent(); //Intent = action to be performed

        //Intent is an object that provides runtime binding (e.g 2 or more activities occurring at one time)

        if (v == btn_menu)
        {
            intent.setClass(this, Mainmenu.class);
            startActivity(intent);
        }
        else if (v == btn_car_left)
        {
            MusicController.Instance.cartype = "Electric Car";
        }
        else if (v == btn_car_right)
        {
            MusicController.Instance.cartype = "Electric Bus";
        }
        else if (v == btn_wep_left)
        {
            switch(MusicController.Instance.CurrentWeapon)
            {
                case PlayerProj_FutureGhost:
                {
                    MusicController.Instance.CurrentWeapon = SampleEntity.ProjType.PlayerProj_JumpingJack;
                }
                break;

                case PlayerProj_Boomerang:
                {
                    MusicController.Instance.CurrentWeapon = SampleEntity.ProjType.PlayerProj_FutureGhost;
                }
                break;

                case PlayerProj_JumpingJack:
                {
                    MusicController.Instance.CurrentWeapon = SampleEntity.ProjType.PlayerProj_Boomerang;
                }
                break;
            }
        }
        else if (v == btn_wep_right)
        {
            switch(MusicController.Instance.CurrentWeapon)
            {
                case PlayerProj_FutureGhost:
                {
                    MusicController.Instance.CurrentWeapon = SampleEntity.ProjType.PlayerProj_Boomerang;
                }
                break;

                case PlayerProj_Boomerang:
                {
                    MusicController.Instance.CurrentWeapon = SampleEntity.ProjType.PlayerProj_JumpingJack;
                }
                break;

                case PlayerProj_JumpingJack:
                {
                    MusicController.Instance.CurrentWeapon = SampleEntity.ProjType.PlayerProj_FutureGhost;
                }
                break;
            }
        }

        if (MusicController.Instance.cartype == "Electric Car")
            vehicleImg.setImageResource(R.drawable.bw_car);
        else if (MusicController.Instance.cartype == "Electric Bus")
            vehicleImg.setImageResource(R.drawable.bw_bus); //change this

        if (MusicController.Instance.CurrentWeapon == SampleEntity.ProjType.PlayerProj_FutureGhost)
            weaponImg.setImageResource(R.drawable.bw_proj);
        else if (MusicController.Instance.CurrentWeapon == SampleEntity.ProjType.PlayerProj_Boomerang)
            weaponImg.setImageResource(R.drawable.bw_proj_boomerang);
        else if (MusicController.Instance.CurrentWeapon == SampleEntity.ProjType.PlayerProj_JumpingJack)
            weaponImg.setImageResource(R.drawable.bw_proj_jumpingjack);
//        else if (v == increaseNum)
//        {
//            Loot += 1;
//            line = Integer.toString(Loot);
//            tv.setText(line); ////Set the text to text view.
//        }
//        else if (v == decreaseNum)
//        {
//            Loot -= 1;
//            line = Integer.toString(Loot);
//            tv.setText(line); ////Set the text to text view.
//        }

    }

//    public void write2File()
//    {
//        //BufferedOutputStream b = new BufferedOutputStream() {
//
//        //};
//
//    }
}
