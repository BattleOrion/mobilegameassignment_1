package com.example.jovanlow.batterygame_mgp2018lab1;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.Log;
import android.view.SurfaceView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Vector;

import static java.lang.StrictMath.abs;

public class EntityManager
{
    public final static EntityManager Instance = new EntityManager();
    private ArrayList<EntityBase> entityList = new ArrayList<EntityBase>();
    private ArrayList<EntityBase> tempaddList = new ArrayList<EntityBase>();
    public HashMap<Integer, Boolean> UniqueUIID = new HashMap<Integer, Boolean>();
    private SurfaceView view = null;
    protected static final String TAG = null;
    public int screenOffset = 800;
    private EntityManager()
    {
    }

    public void Init(SurfaceView _view)
    {
        view = _view;
    }

    public void Update(float _dt)
    {
        ArrayList<EntityBase> removalList = new ArrayList<>();

        // Update all
        for(Iterator<EntityBase> it = entityList.iterator(); it.hasNext();) {
            EntityBase go = it.next();
            if (go != null) {
                go.Update(_dt);

                SampleEntity th = (SampleEntity)go;
                if (th.xPos <= -screenOffset || th.xPos >= screenOffset + Batterygame.Instance.screenWidth || th.yPos <= -screenOffset || th.yPos >= screenOffset + Batterygame.Instance.screenHeight)
                {
                    System.out.println("Deleting an Out of Bound..");
                    go.SetIsDone(true);
                }

                // Check if need to clean up
                if (go.IsDone())
                    removalList.add(go);
            }
        }
        //for(Iterator<EntityBase> it = entityList.iterator(); it.hasNext();) {
        //EntityBase go = it.next();
        //if (go.IsDone()) {
        //   it.remove();
        // }
        // }
        // Remove all entities that are done
        //entityList.removeAll(removalList);
        removalList.clear();

        // Collision Check
        for (int i = 0; i < entityList.size(); ++i)
        {
            EntityBase currEntity = entityList.get(i);

            //if (currEntity instanceof Collidable)
            //{
                //Collidable first = (Collidable) currEntity;

                for (int j = i+1; j < entityList.size(); ++j)
                {
                    EntityBase otherEntity = entityList.get(j);

                    //if (otherEntity instanceof Collidable)
                    {
                        //Collidable second = (Collidable) otherEntity;

                        //if (Collision.SphereToSphere(first.GetPosX(), first.GetPosY(), first.GetRadius(), second.GetPosX(), second.GetPosY(), second.GetRadius()))
                        //{
                           // first.OnHit(second);
                           // second.OnHit(first);
                        //}

                        SampleEntity go0 = (SampleEntity)currEntity;
                        SampleEntity go1 = (SampleEntity)otherEntity;

                        if (go0.spritesheet != null && go1.spritesheet != null) {

                            int currW = (go0.spritesheet.bmp.getWidth() / 2) / go0.spritesheet.col;
                            int currH = (go0.spritesheet.bmp.getHeight() / 2) / go0.spritesheet.row;
                            int otherW = (go1.spritesheet.bmp.getWidth() / 2) / go0.spritesheet.col;
                            int otherH = (go1.spritesheet.bmp.getHeight() / 2) / go0.spritesheet.row;

                            //System.out.println(go0.type + " " + go1.type);

                            if (Collision.AABBCollision(go0.xPos, go0.yPos, go0.zPos, go1.xPos, go1.yPos, go1.zPos, -currW, -currH, -(int) (go0.zLength / 2), currW, currH, (int) (go0.zLength / 2), -otherW, -otherH, -(int) (go1.zLength / 2), otherW, otherH, (int) (go1.zLength / 2))) {
                                go0.Collided(go1);
                                go1.Collided(go0);
                            }

                            if (abs(go0.zPos - go1.zPos) < 2) {
                                go0.SharedLane(go1);
                                go1.SharedLane(go0);
                            }

                        }

                    }
                }
            //}

            // Check if need to clean up
            //if (currEntity.IsDone())
                //removalList.add(currEntity);
        }
        //for(Iterator<EntityBase> it = entityList.iterator(); it.hasNext();) {
           // EntityBase go = it.next();
            //if (go.IsDone()) {
          //      it.remove();
        //    }
      // }

        // Remove all entities that are done
        //for (EntityBase currEntity : removalList) {
        //   entityList.remove(currEntity);
        // }

        //entityList.removeAll(removalList);

        ArrayList<EntityBase> UIRemovalList = new ArrayList<>();
        Iterator<EntityBase> iter = entityList.iterator();
        while(iter.hasNext()){
            EntityBase str = iter.next();
            if( str.IsDone()){

                SampleEntity ent = (SampleEntity)str;

                if (UniqueUIID.containsKey(ent.uiPairID))
                {
                    Iterator<EntityBase> iter2 = entityList.iterator();
                    while(iter2.hasNext()){
                        EntityBase str2 = iter2.next();
                        SampleEntity ent2 = (SampleEntity)str2;
                        if (ent2.uiPairID == ent.uiPairID && ent2 != ent)
                        {
                            //ent2.SetIsDone(true);
                            UIRemovalList.add(str2);
                            break;
                        }
                    }

                    UniqueUIID.remove(ent.uiPairID);
                }
                DeleteEntityMesh(ent);
                iter.remove();
            }
        }
        for (EntityBase currEntity : UIRemovalList) {
            DeleteEntityMesh((SampleEntity)currEntity);
           entityList.remove(currEntity);
         }
        UIRemovalList.clear();


        removalList.clear();

        iter = tempaddList.iterator();
        while(iter.hasNext()){
            EntityBase str = iter.next();
            entityList.add(str);
        }
        tempaddList.clear();

        //sort in increasing z value
        for (int i = 1; i < entityList.size(); ++i)
        {
            for (int j = 0; j < entityList.size() - i; j++)
            {
                if (((SampleEntity)entityList.get(j)).zPos > ((SampleEntity)entityList.get(j + 1)).zPos)
                {
                    Collections.swap(entityList, j, j + 1);
                }
            }
        }

    }

    public void Render(Canvas _canvas)
    {
        for(Iterator<EntityBase> it = entityList.iterator(); it.hasNext();) {
                EntityBase go = it.next();
                {
                    go.Render(_canvas);
                }
            //Log.v(TAG, "AAAAAAAAAAAAAAAAAAAA");
        }
    }

    public void AddEntity(EntityBase _newEntity)
    {
        _newEntity.Init(view);
        //entityList.add(_newEntity);
        tempaddList.add(_newEntity);

    }

    public void DoneAllThat()
    {
        for (EntityBase currEntity : entityList) {
            //DeleteEntityMesh((SampleEntity)currEntity);
            //entityList.remove(currEntity);
            currEntity.SetIsDone(true);
        }
    }

    public int CaptureUnleash()
    {
        int captured = 0;

        for(Iterator<EntityBase> it = entityList.iterator(); it.hasNext();) {
            SampleEntity go = (SampleEntity)(it.next());
            {
                if (go.state == SampleEntity.State.Capturable)
                {
                    go.SetIsDone(true);
                    ++captured;
                    MusicController.Instance.CurrentGameScore += 550;
                }
            }
        }

        return captured;
    }

    public boolean EnemyPresent()
    {
        for(Iterator<EntityBase> it = entityList.iterator(); it.hasNext();) {
            SampleEntity go = (SampleEntity)(it.next());
            {
                if (go.type == SampleEntity.Type.FossilCar)
                {
                    return true;
                }
            }
        }
        return false;
    }

    public void DamageAllExceptPlayer(float dmgval)
    {
        for(Iterator<EntityBase> it = entityList.iterator(); it.hasNext();) {
            SampleEntity go = (SampleEntity)(it.next());
            {
                if (go.type != SampleEntity.Type.ElectricCar)
                {
                    go.hp -= dmgval;
                }
            }
        }
    }
    public void DeleteEntityMesh(SampleEntity ent)
    {
        if (ent.mesh != null) ent.mesh.recycle();
        if (ent.playerLandingEffect != null) ent.playerLandingEffect.recycle();
        if (ent.attackEffect != null) ent.attackEffect.recycle();
        if (ent.cancaptureEffect != null) ent.cancaptureEffect.recycle();

        if (ent.spritesheet != null) ent.spritesheet.bmp.recycle();
        if (ent.normal_spritesheet != null) ent.normal_spritesheet.bmp.recycle();
        if (ent.jumping_spritesheet != null) ent.jumping_spritesheet.bmp.recycle();
        if (ent.attacking_spritesheet != null) ent.attacking_spritesheet.bmp.recycle();
        if (ent.attackingspecial_spritesheet != null) ent.attackingspecial_spritesheet.bmp.recycle();
        if (ent.dead_spritesheet != null) ent.dead_spritesheet.bmp.recycle();
        if (ent.capturable_spritesheet != null) ent.capturable_spritesheet.bmp.recycle();

    }

}

