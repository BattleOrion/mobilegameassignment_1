package com.example.jovanlow.batterygame_mgp2018lab1;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.MediaPlayer;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceView;
import android.os.Build;

import java.util.Random;

import static java.lang.StrictMath.abs;

public class SampleEntity implements EntityBase, Collidable
{
    public enum State
    {
        Normal,
        Jumping,
        Attacking,
        Attacking_Special,
        Capturable,
        Dead,
        StateTotal
    }
    Sprite normal_spritesheet = null;
    Sprite jumping_spritesheet = null;
    Sprite attacking_spritesheet = null;
    Sprite attackingspecial_spritesheet = null;
    Sprite capturable_spritesheet = null;
    Sprite dead_spritesheet = null;

    public enum Type
    {
        ElectricCar,
        FossilCar,
        Projectile,
        Effect,
        BGObj,
        TypeTotal
    }
    public enum ProjType
    {
        Proj_None,
        PlayerProj_FutureGhost,
        PlayerProj_Boomerang,
        PlayerProj_JumpingJack, //jumps up and down in the direction it is launched in
        ProjTypeTotal
    }
    boolean canCapture = false, canSpecial = false;
    float captureChance = 0;
    float specialCharge = 0, specialChargeMax = 100;
    int captured = 0;
    State state = State.Normal;
    Type type = Type.ElectricCar;
    ProjType projtype = ProjType.Proj_None;
    ProjType Weapon = ProjType.Proj_None;
	//Init any variables here

    //SampleEntity specialUIPointer = null;

    int uiPairID = 0;

    float hp = 3, maxhp = 3, XSpeed = 0, YSpeed = 0, ZSpeed = 0, XAccel = 0, YAccel = 0, ZAccel = 0;
    float damage = 1;
    double bounceTime = 0.0, stateTime = 0.0, existTime = 0.0;
    float xPos = 0, yPos = 0, zPos = 0;
    private boolean isDone = false;
    float zLength = 2.0f;

    int ScreenWidth, ScreenHeight;
    Bitmap mesh = null;
    Sprite spritesheet = null;

    Bitmap playerLandingEffect = null, attackEffect = null, cancaptureEffect = null;

    protected static final String TAG = null;

    @Override
    public boolean IsDone() {
        return isDone;
    }

    @Override
    public void SetIsDone(boolean _isDone) {
        isDone = _isDone;
    }

    public void SetMesh (Bitmap _mesh) {mesh = _mesh;}

    public void SetSpriteSheet (Bitmap _mesh, int row, int col, int fps)
    {
        spritesheet = new Sprite(_mesh, row, col, fps);
    }

    public void SetSpriteSheetNormal (Bitmap _mesh, int row, int col, int fps)
    {
        normal_spritesheet = new Sprite(_mesh, row, col, fps);
    }

    public void SetSpriteSheetJump (Bitmap _mesh, int row, int col, int fps)
    {
        jumping_spritesheet = new Sprite(_mesh, row, col, fps);
    }

    public void SetSpriteSheetAttack (Bitmap _mesh, int row, int col, int fps)
    {
        attacking_spritesheet = new Sprite(_mesh, row, col, fps);
    }

    public void SetSpriteSheetAttackSpecial (Bitmap _mesh, int row, int col, int fps)
    {
        attackingspecial_spritesheet = new Sprite(_mesh, row, col, fps);
    }

    public void SetSpriteSheetCapture (Bitmap _mesh, int row, int col, int fps)
    {
        capturable_spritesheet = new Sprite(_mesh, row, col, fps);
    }

    public void SetSpriteSheetDead (Bitmap _mesh, int row, int col, int fps)
    {
        dead_spritesheet = new Sprite(_mesh, row, col, fps);
    }

    @Override
    public void Init(SurfaceView _view) {
        
		// Define anything you need to use here

        //spritesheet = new Sprite(BitmapFactory.decodeResource(_view.getResources(), R.drawable.bw_car), 1, 1, 12);


        DisplayMetrics metrics = _view.getResources().getDisplayMetrics();
        ScreenWidth = metrics.widthPixels;
        ScreenHeight = metrics.heightPixels;

        playerLandingEffect = BitmapFactory.decodeResource(_view.getResources(), R.drawable.bw_effect_landing);
        attackEffect = BitmapFactory.decodeResource(_view.getResources(), R.drawable.bw_effect_hit);
        cancaptureEffect = BitmapFactory.decodeResource(_view.getResources(), R.drawable.bw_cancapture);
    }

    @Override
    public void Update(float delta)
	{
        // Update based on dt

        float _dt = delta * Time.Instance.entitySpeed;

        if (spritesheet != null) spritesheet.Update(_dt);
        xPos += XSpeed * delta;
        yPos += YSpeed * delta;
        zPos += ZSpeed * delta;
        XSpeed += XAccel * delta;
        YSpeed += YAccel * delta;
        ZSpeed += ZAccel * delta;
        if (Time.Instance.entitySpeed <= 0)
        {
            XSpeed -= XAccel * delta;
            YSpeed -= YAccel * delta;
            ZSpeed -= ZAccel * delta;
            xPos -= XSpeed * delta;
            yPos -= YSpeed * delta;
            zPos -= ZSpeed * delta;
        }
        bounceTime -= _dt;
        stateTime -= _dt;
        //existTime -= _dt;

        if (type == Type.Projectile || type == Type.Effect)
        {
            existTime -= _dt;

            if (type == Type.Projectile && projtype == ProjType.PlayerProj_Boomerang)
            {
                if (existTime > 0 && existTime <= 1.0)
                {
                    ZAccel = -4 * ZAccel;
                    existTime = 0;
                }
                if (existTime > -2.0 && existTime <= -1.0)
                {
                    YAccel = 500;
                    existTime = -2.0;
                }
            }
            else if (type == Type.Projectile && projtype == ProjType.PlayerProj_JumpingJack)
            {
                if (yPos >= 700.0f)
                {
                    yPos = 700.0f;
                    YSpeed = -400;
                }

                if (existTime < 0.0)
                {
                    SetIsDone(true);
                }
            }
            else
            {
                if (existTime < 0.0)
                {
                    SetIsDone(true);
                }
            }
        }

        if (state == State.Attacking && stateTime <= 0.0)
        {
            state = State.Normal;
            if (normal_spritesheet != null && spritesheet != normal_spritesheet)
            {
                spritesheet = normal_spritesheet;
            }
            XSpeed = 0;
            YSpeed = 0;
            ZSpeed = 0;
            XAccel = 0;
            YAccel = 0;
            ZAccel = 0;
        }

        if (state == State.Jumping && stateTime <= 0.0)
        {
            //System.out.println("the"  +zPos);
            state = State.Normal;
            if (normal_spritesheet != null && spritesheet != normal_spritesheet)
            {
                spritesheet = normal_spritesheet;
            }

            if (ZSpeed >= 0.0)
            {
                zPos = Batterygame.roadLowerDist;
            }
            else
            {
                zPos = Batterygame.roadUpperDist;
            }
            yPos = Batterygame.groundLevel;
            XSpeed = 0;
            YSpeed = 0;
            ZSpeed = 0;
            XAccel = 0;
            YAccel = 0;
            ZAccel = 0;

            Matrix matrix = new Matrix();
            SampleEntity proj = SampleEntity.Create();
            matrix.postScale(0.5f, 0.5f);

            proj.SetSpriteSheet(Bitmap.createBitmap(playerLandingEffect, 0, 0, playerLandingEffect.getWidth(), playerLandingEffect.getHeight(), matrix, true), 2, 3, 12);

            proj.xPos = xPos;
            proj.yPos = yPos;
            proj.zPos = zPos;
            proj.existTime = 0.4;
            proj.type = Type.Effect;
            matrix.postScale(2, 2);
        }

        if (TouchManager.Instance.HasTapped())
        {
            float imgRadius = spritesheet.GetHeight() * 0.5f;
            if (Collision.SphereToSphere(TouchManager.Instance.GetTouchPosX(), TouchManager.Instance.GetTouchPosY(), 10.0f, xPos, yPos, imgRadius))
            {
                //SetIsDone(true);
            }

        }

        if (hp <= 0 && state != State.Dead && state != State.Capturable)
        {
            float randomfloat = new Random().nextFloat() * 100;

            if (canCapture && randomfloat < captureChance)
            {
                state = State.Capturable;

                XAccel = 0;
                YAccel = 0;
                ZAccel = 0;

                SampleEntity proj = SampleEntity.Create();
                Matrix matrix = new Matrix();
                matrix.postScale(0.4f, 0.4f);
                proj.SetSpriteSheetNormal(Bitmap.createBitmap(cancaptureEffect, 0, 0, cancaptureEffect.getWidth(), cancaptureEffect.getHeight(), matrix, true), 2, 3, 12);
                proj.spritesheet = proj.normal_spritesheet;
                proj.xPos = xPos;
                proj.yPos = yPos + 30;
                proj.zPos = zPos;
                proj.existTime = 5.0;
                proj.type = SampleEntity.Type.Effect;
                proj.XSpeed = XSpeed;
                proj.YSpeed = YSpeed;
                proj.ZSpeed = ZSpeed;
                matrix.postScale(1/0.4f, 1/0.4f);

                int randomKey = new Random().nextInt() * 99999999;
                while (randomKey == 0 || EntityManager.Instance.UniqueUIID.containsKey(randomKey))
                {
                    randomKey = new Random().nextInt() * 99999999;
                }
                EntityManager.Instance.UniqueUIID.put(randomKey, true);
                uiPairID = randomKey;
                proj.uiPairID = randomKey;

                stateTime = 5.0;
                if (capturable_spritesheet != null && spritesheet != capturable_spritesheet)
                {
                    spritesheet = capturable_spritesheet;
                }
            }
            else {
                state = State.Dead;
                stateTime = 1.0;
                XSpeed = -Batterygame.Instance.roads.velX;
                YSpeed = 0;
                ZSpeed = 0;
                XAccel = 0;
                YAccel = 0;
                ZAccel = 0;
                if (dead_spritesheet != null && spritesheet != dead_spritesheet)
                {
                    spritesheet = dead_spritesheet;
                }
            }
        }
        if ( state == State.Capturable && stateTime <= 0.0)
        {
            state = State.Dead;
            stateTime = 1.0;
            XSpeed = -Batterygame.Instance.roads.velX;
            YSpeed = 0;
            ZSpeed = 0;
            XAccel = 0;
            YAccel = 0;
            ZAccel = 0;
            if (dead_spritesheet != null && spritesheet != dead_spritesheet)
            {
                spritesheet = dead_spritesheet;
            }
        }
        if ( state == State.Dead && stateTime <= 0.0)
        {
            if (!MusicController.Instance.SoundEffectsControl[0].isPlaying()) {
                if (MusicController.Instance.SoundEffectsControl[0] != null) {
                    MusicController.Instance.SoundEffectsControl[0].reset();
                    MusicController.Instance.SoundEffectsControl[0].release();
                    MusicController.Instance.SoundEffectsControl[0] = null;
                }
                MusicController.Instance.SoundEffectsControl[0] = MediaPlayer.create(MusicController.Instance.gamepageContext, R.raw.snd_death);
                {//'99' is current volume level
                    MusicController.Instance.SoundEffectsControl[0].start();
                    float volume = (float) (1 - (Math.log(100 - MusicController.Instance.SoundVolume) / Math.log(100)));
                    MusicController.Instance.SoundEffectsControl[0].setVolume(volume, volume);
                }
            }

            if (type == Type.FossilCar)
            MusicController.Instance.CurrentGameScore += 200;

            SetIsDone(true);
        }

        switch(type)//exclusive things, mainly for special moves
        {
            case FossilCar:
            {
                if (canSpecial) {
                    if (specialCharge >= specialChargeMax && state != State.Dead && state != State.Jumping && state != State.Capturable && state != State.Attacking_Special) {
                        stateTime = 0.6;
                        state = State.Attacking_Special;
                        if (attackingspecial_spritesheet != null && spritesheet != attackingspecial_spritesheet) {
                            spritesheet = attackingspecial_spritesheet;
                        }
                    }
                    if (state == State.Attacking_Special) {
                        if (stateTime <= 0 && abs(XSpeed) <= 100) {
                            XSpeed *= 4;
                            XAccel = XSpeed / 2;
                        }
                    }
                }
            }
                break;
            default:
                break;
        }

    }

    @Override
    public void Render(Canvas _canvas)
    {
        // Render anything

        if (spritesheet!= null) spritesheet.Render(_canvas, (int)xPos, (int)yPos, (int)zPos);
        //if (mesh != null)
		//_canvas.drawBitmap(mesh, xPos + zPos * 0.2f, yPos + zPos, null);
    }

    public static SampleEntity Create()
    {
        SampleEntity result = new SampleEntity();
        EntityManager.Instance.AddEntity(result);
        return result;
    }

    @Override
    public String GetType() {
        return "SampleEntity";
    }

    @Override
    public float GetPosX() {
        return xPos;
    }

    @Override
    public float GetPosY() {
        return yPos;
    }

    @Override
    public float GetPosZ() {
        return zPos;
    }

    public float GetRadius()
    {
        return spritesheet.GetHeight() * 0.5f;
    }

    @Override
    public void OnHit(Collidable _other)
    {
        if (_other.GetType() == "SampleEntity")
        {
            //SetIsDone(true);
        }
    }

    public void SharedLane(SampleEntity _other)
    {
        if (type == Type.FossilCar)
        {
            if (_other.type == Type.ElectricCar) {
                specialCharge = specialChargeMax;
            }
        }
    }

    public void Collided(SampleEntity _other)
    {
        if (type == Type.ElectricCar)
        {
            if (_other.type == Type.FossilCar && _other.state != State.Capturable && _other.state != State.Dead)
            {
                if (bounceTime <= 0.0) {
                    hp -= _other.damage;
                    bounceTime = 1.0;

                    if (!MusicController.Instance.SoundEffectsControl[0].isPlaying()) {

                        if (MusicController.Instance.SoundEffectsControl[0] != null) {
                            MusicController.Instance.SoundEffectsControl[0].reset();
                            MusicController.Instance.SoundEffectsControl[0].release();
                            MusicController.Instance.SoundEffectsControl[0] = null;
                        }
                        MusicController.Instance.SoundEffectsControl[0] = MediaPlayer.create(MusicController.Instance.gamepageContext, R.raw.snd_hit);
                        {//'99' is current volume level
                            MusicController.Instance.SoundEffectsControl[0].start();
                            float volume = (float) (1 - (Math.log(100 - MusicController.Instance.SoundVolume) / Math.log(100)));
                            MusicController.Instance.SoundEffectsControl[0].setVolume(volume, volume);
                        }
                    }

                }
            }
        }
        else if (type == Type.FossilCar)
        {
            if (_other.type == Type.Projectile)
            {
                if (bounceTime <= 0.0) {

                    if (_other.projtype == ProjType.PlayerProj_FutureGhost) {
                        hp -= 1;
                        _other.SetIsDone(true);

                        if (!MusicController.Instance.SoundEffectsControl[0].isPlaying()) {

                            if (MusicController.Instance.SoundEffectsControl[0] != null) {
                                MusicController.Instance.SoundEffectsControl[0].reset();
                                MusicController.Instance.SoundEffectsControl[0].release();
                                MusicController.Instance.SoundEffectsControl[0] = null;
                            }
                            MusicController.Instance.SoundEffectsControl[0] = MediaPlayer.create(MusicController.Instance.gamepageContext, R.raw.snd_hit);
                            {//'99' is current volume level
                                MusicController.Instance.SoundEffectsControl[0].start();
                                float volume = (float) (1 - (Math.log(100 - MusicController.Instance.SoundVolume) / Math.log(100)));
                                MusicController.Instance.SoundEffectsControl[0].setVolume(volume, volume);
                            }
                        }
                    }
                    else if (_other.projtype == ProjType.PlayerProj_Boomerang || _other.projtype == ProjType.PlayerProj_JumpingJack) {
                        hp -= 1;

                        if (!MusicController.Instance.SoundEffectsControl[0].isPlaying()) {

                            if (MusicController.Instance.SoundEffectsControl[0] != null) {
                                MusicController.Instance.SoundEffectsControl[0].reset();
                                MusicController.Instance.SoundEffectsControl[0].release();
                                MusicController.Instance.SoundEffectsControl[0] = null;
                            }
                            MusicController.Instance.SoundEffectsControl[0] = MediaPlayer.create(MusicController.Instance.gamepageContext, R.raw.snd_hit);
                            {//'99' is current volume level
                                MusicController.Instance.SoundEffectsControl[0].start();
                                float volume = (float) (1 - (Math.log(100 - MusicController.Instance.SoundVolume) / Math.log(100)));
                                MusicController.Instance.SoundEffectsControl[0].setVolume(volume, volume);
                            }
                        }
                    }
                    bounceTime = 0.3;

                    Matrix matrix = new Matrix();
                    SampleEntity proj = SampleEntity.Create();
                    matrix.postScale(0.5f, 0.5f);

                    proj.SetSpriteSheet(Bitmap.createBitmap(attackEffect, 0, 0, attackEffect.getWidth(), attackEffect.getHeight(), matrix, true), 2, 3, 12);

                    proj.xPos = _other.xPos;
                    proj.yPos = _other.yPos;
                    proj.zPos = _other.zPos;
                    proj.existTime = 0.4;
                    proj.type = Type.Effect;
                    matrix.postScale(2, 2);
                }
            }
        }
    }

}

