package com.example.jovanlow.batterygame_mgp2018lab1;

import java.util.LinkedList;

public class LevelPacket {

    Level.DataType datatype = Level.DataType.ENEMY;
    Level.Condition condition = Level.Condition.NO_ENEMY;

    LinkedList<EnemyData> enemylist = new LinkedList<>();

    int musictrack = R.raw.neptune;

    double duration = 4.0;

    String dialogue = "Missing Dialogue";

    public LevelPacket(Level.DataType datatype_, Level.Condition condition_) //next time, also remember to add more trigger conditions
    {
        datatype = datatype_;
        condition = condition_;
    }

}
