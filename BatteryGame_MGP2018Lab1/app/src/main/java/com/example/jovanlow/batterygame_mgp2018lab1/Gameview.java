package com.example.jovanlow.batterygame_mgp2018lab1;

import android.content.Context;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

//Making this class a SurfaceView class
public class Gameview extends SurfaceView {

    private SurfaceHolder holder = null;

    private Updatethread updatethread = new Updatethread(this);

    public Gameview(Context _context)
    {
        super (_context);
        holder = getHolder();


        //CONTEXT IS NEEDED FOR STUFF LIKE THIS! Might wanna use it for etc stuff too like text?
        //Could make more controllers that use this context i suppose
        MusicController.Instance.gamepageContext = _context;
        MusicController.Instance.text = Typeface.createFromAsset(MusicController.Instance.gamepageContext.getAssets(), "fonts/gemcut.otf");

        MusicController.Instance.Init();
        Batterygame.Instance.StartLevel();

        //game info?
        if(holder != null)
        {
            holder.addCallback(new SurfaceHolder.Callback() {
                @Override
                public void surfaceCreated(SurfaceHolder holder) {
                    //Setup stuff to start Updatethread
                    if (!updatethread.IsRunning())
                    {
                        updatethread.Initialized();
                    }

                    if (!updatethread.isAlive())
                    {
                        updatethread.start();
                    }

                }

                @Override
                public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                    //Do nothing because Updatethread will handle it

                }

                @Override
                public void surfaceDestroyed(SurfaceHolder holder) {
                    //Terminate the thread
                    updatethread.Terminate();
                }
            });
        }

    }

}
