package com.example.jovanlow.batterygame_mgp2018lab1;

import android.content.Context;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.net.Uri;

public class MusicController {
    public final static MusicController Instance = new MusicController();

    Context gamepageContext = null;

    String cartype = "Electric Car";

    float[] LeaderBoard = new float[5];

    SampleEntity.ProjType CurrentWeapon = SampleEntity.ProjType.PlayerProj_Boomerang;

    MediaPlayer MusicControl;
    Typeface text;
    double MusicVolume = 99; //0-100
    double SoundVolume = 99; //0-100

    float CurrentGameScore = 0;

    int totalSoundEffect = 16;
    //I only use soundeffectscontrol[0] because of some weird thing
    MediaPlayer[] SoundEffectsControl = new MediaPlayer[totalSoundEffect]; //only some sound effects can exist at once

    String leveltextfile = "level1.txt";

    public MusicController() {
        LeaderBoard[0] = 99999;
        LeaderBoard[1] = 500;
        LeaderBoard[2] = 400;
        LeaderBoard[3] = 250;
        LeaderBoard[4] = 0;
    }

    public void Init()
    {
        if (gamepageContext != null) {
            for (int i = 0; i < totalSoundEffect; ++i) {
                SoundEffectsControl[i] = MediaPlayer.create(gamepageContext, R.raw.snd_hit);
            }
        }
    }

    public void Update()
    {
        if (gamepageContext != null) {
            float volume = 0;

            if (MusicControl != null) {
                volume = (float) (1 - (Math.log(100 - MusicController.Instance.MusicVolume) / Math.log(100)));
                MusicControl.setVolume(volume, volume);
            }

            volume = (float) (1 - (Math.log(100 - MusicController.Instance.SoundVolume) / Math.log(100)));
            for (int i = 0; i < totalSoundEffect; ++i) {
                if (SoundEffectsControl[i] != null) {
                    SoundEffectsControl[i].setVolume(volume, volume);
                }
            }
        }
    }

    public void Stop()
    {
        if (MusicControl != null)
        {
            MusicControl.stop();
            for (int i = 0; i < totalSoundEffect; ++i) {
                if (SoundEffectsControl[i] != null) {
                    SoundEffectsControl[i].stop();
                }
            }
        }
    }

    int musicToChangeTo = 0;
    boolean changeMusicbool = false;

    void ChangeMusic(int music)
    {
        musicToChangeTo = music;
        changeMusicbool = true;
    }

}
