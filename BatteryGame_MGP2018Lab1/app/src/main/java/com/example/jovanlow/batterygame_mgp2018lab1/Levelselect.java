package com.example.jovanlow.batterygame_mgp2018lab1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

public class Levelselect extends Activity implements OnClickListener {

    private Button btn_lvl1, btn_lvl2;


    //Annotation to assure that the subclass method is overriding the parent class method. If it does not, compile with error
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //code for hiding UI
        requestWindowFeature(Window.FEATURE_NO_TITLE); //hide title
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN); //hide top bar

        setContentView(R.layout.levelselect);

        //Set listener to button
        btn_lvl1 = (Button) findViewById(R.id.lvl1);
        btn_lvl1.setOnClickListener(this);
        btn_lvl2 = (Button) findViewById(R.id.lvl2);
        btn_lvl2.setOnClickListener(this);
    }

    @Override // will happen if there is an tap of the button on the screen/view
    public void onClick(View v)
    {
        Intent intent = new Intent(); //Intent = action to be performed

        //Intent is an object that provides runtime binding (e.g 2 or more activities occurring at one time)

        if (v == btn_lvl1)
        {
            intent.setClass(this, Gamepage.class);
            MusicController.Instance.leveltextfile = "level1.txt";
            //Batterygame.Instance.StartLevel();
            startActivity(intent);
        }
        else if (v == btn_lvl2)
        {
            intent.setClass(this, Gamepage.class);
            MusicController.Instance.leveltextfile = "level2.txt";
            //Batterygame.Instance.StartLevel();
            startActivity(intent);
        }

    }

}
