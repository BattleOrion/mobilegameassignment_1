package com.example.jovanlow.batterygame_mgp2018lab1;

import android.util.Log;
import android.view.MotionEvent;

import static java.lang.StrictMath.abs;

public class TouchManager {

    public final static TouchManager Instance = new TouchManager();

    private TouchManager()
    {

    }

    public enum TouchState
    {
        NONE,
        DOWN,
        MOVE,
        UP
    }

    public enum SwipeDirection
    {
        UP,
        LEFT,
        DOWN,
        RIGHT,
        NONE
    }

    public SwipeDirection dir = SwipeDirection.NONE;

    private int touchX = 0, touchY = 0;
    private TouchState status = TouchState.NONE;
    private boolean tapped = true;

    //bootleg swipe
    private double heldRemainingTime = 0.0;
    private int heldX = 0, heldY = 0;
    public boolean held = false;

    public int heldDownX = 0, heldDownY = 0;
    public int prevTouchX = 0, prevTouchY = 0;

    public void Update(double _dt)
    {
        heldRemainingTime -= _dt;

        if (held && heldRemainingTime <= 0.0 && dir != SwipeDirection.NONE)
        {
            dir = SwipeDirection.NONE;
        }
        //System.out.println(dir);
    }

    public boolean HasTouch() //check for a touch status on screen
    {

        return status == TouchState.DOWN || status == TouchState.MOVE || status == TouchState.UP;
    }

    public boolean HasTapped()
    {
        if (!tapped && status == TouchState.DOWN)
        {
            return true;
        }
        return false;
    }

    public boolean IsDown()
    {
        return status == TouchState.DOWN;
    }

    public int GetTouchPosX()
    {
        return touchX;
    }
    public int GetTouchPosY()
    {
        return touchY;
    }
    public void Update(int _touchX, int _touchY, int _motionEventStatus)
    {
        prevTouchX = touchX;
        prevTouchY = touchY;

        if (status == TouchState.DOWN)
        {
            tapped = true;
        }
        touchX = _touchX;
        touchY = _touchY;

        switch (_motionEventStatus)
        {
            case MotionEvent.ACTION_DOWN:
            {
                status = TouchState.DOWN;
                if (!held)
                {
                    held = true;
                    heldX = touchX;
                    heldY = touchY;
                    heldRemainingTime = 0.5;
                    dir = SwipeDirection.NONE;
                }
                break;
            }
            case MotionEvent.ACTION_MOVE:
            {
                heldDownX = touchX - prevTouchX;
                heldDownY = touchY - prevTouchY;

                System.out.println(heldDownX + " " + heldDownY);

                if (held && heldRemainingTime > 0 || dir == SwipeDirection.NONE)
                {
                    int xDist = heldDownX;
                    int yDist = heldDownY;

                    if (xDist * xDist + yDist * yDist > 20 * 20) {
                        if (abs(xDist) >= abs(yDist)) {
                            if (xDist >= 0) {
                                dir = SwipeDirection.RIGHT;
                            } else {
                                dir = SwipeDirection.LEFT;
                            }
                        } else {
                            if (yDist >= 0) {
                                dir = SwipeDirection.DOWN;
                            } else {
                                dir = SwipeDirection.UP;
                            }
                        }
                        heldRemainingTime = 0.5;
                    }
                }

                status = TouchState.MOVE;
                break;
            }
            case MotionEvent.ACTION_UP:
            {
                tapped = false;
                held = false;
                status = TouchState.UP;
                break;
            }
            default:  status = TouchState.NONE;
            break;
        }

    }

}
