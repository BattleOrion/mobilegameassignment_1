package com.example.jovanlow.batterygame_mgp2018lab1;

import java.util.LinkedList;

public class Level {

    public final static Level Instance = new Level();

    public enum DataType
    {
        NONE,
        ENEMY,
        MUSIC,
        DIALOGUE,
        WINGAME,
        LOSEGAME,
        DATATOTAL
    }

    public enum Condition
    {
        NO_ENEMY,
        TIME_PASSED,
        NONE
    }

    public LinkedList<LevelPacket> Level1 = new LinkedList<>(); //temporarily hard-coded.. sorta


    public Level()
    {

    }

//    public String Level1 = "<ENEMY fossilcar:0,0,0,2,1"
//    + " fossilcar:2,2,0,2,1"
//    + " fossilcar:8,2,1,3,2"
//    + " fossilcar:8,3,1,3,2>"
//    + "<ENEMY fossilcar:0,0,0,2,1"
//            + " fossilcar:1,2,0,2,1"
//            + " fossilcar:2,1,0,2,1"
//            + " fossilcar:3,3,0,2,1"
//            + " fossilcar:8,0,1,1,2"
//            + " fossilcar:8,2,1,1,2"
//            + " fossilcar:8,1,1,1,2"
//            + " fossilcar:8,3,1,1,2>";

}
