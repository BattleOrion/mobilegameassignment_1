package com.example.jovanlow.batterygame_mgp2018lab1;


import android.app.Activity;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

public class Gamepage extends Activity {
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); //hide title
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN); //hide top bar
        setContentView(new Gameview(this));

        //MusicController.Instance.gamepageContext = this;

//        MusicController.Instance.MusicControl = MediaPlayer.create(this, R.raw.centurion); //maybe use hashmap for diff soundeffects with more mediaplayers?
//        {//'99' is current volume level
//            MusicController.Instance.MusicControl.start();
//            float volume = (float) (1 - (Math.log(100 - 99) / Math.log(100)));
//            MusicController.Instance.MusicControl.setVolume(volume, volume);
//        }
    }


}
