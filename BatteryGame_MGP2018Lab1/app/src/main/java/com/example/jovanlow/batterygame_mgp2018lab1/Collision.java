package com.example.jovanlow.batterygame_mgp2018lab1;

import java.lang.Math;


public class Collision {

    public static boolean AABBCollision(
            float xPos0, float yPos0, float zPos0, float xPos1, float yPos1, float zPos1
            , float xMin0, float yMin0, float zMin0, float xMax0, float yMax0, float zMax0
    ,float xMin1, float yMin1, float zMin1, float xMax1, float yMax1, float zMax1)
    {
        if (xPos0 + xMax0 < xPos1 + xMin1) return false;
        if (xPos0 + xMin0 > xPos1 + xMax1) return false;
        if (yPos0 + yMax0 < yPos1 + yMin1) return false;
        if (yPos0 + yMin0 > yPos1 + yMax1) return false;
        if (zPos0 + zMax0 < zPos1 + zMin1) return false;
        if (zPos0 + zMin0 > zPos1 + zMax1) return false;

        return true;
    }

    public static boolean SphereToSphere(float x0, float y0, float rad0, float x1, float y1, float rad1)
    {
        float xVec = x1 - x0;
        float yVec = y1 - y0;

        float distSquared = xVec * xVec + yVec * yVec;

        float rSquared = rad0 + rad1;
        rSquared *= rSquared;



        if (distSquared > rSquared) return false;
        return true;

    }

}
