package com.example.jovanlow.batterygame_mgp2018lab1;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import java.util.List;
import java.util.Arrays;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.login.widget.ProfilePictureView;
import com.facebook.share.ShareApi;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareButton;
import com.facebook.share.widget.ShareDialog;
import com.facebook.share.model.ShareLinkContent;

import android.net.Uri;

public class Help extends Activity implements OnClickListener {

    //Define an object and pass it to another method to use
    private Button btn_menu;

    private Button btn_fbLogin;

    boolean loggedin = false;
    private CallbackManager callbackManager;
    private LoginManager loginManager;

    private static final String EMAIL = "email";

    //ShareDialog shareDialog;
    ProfilePictureView profile_pic;
    List<String> PERMISSIONS = Arrays.asList("publish_actions");

    private ShareButton shareButton;
    private ShareDialog shareDialog;

    //Annotation to assure that the subclass method is overriding the parent class method. If it does not, compile with error
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //code for hiding UI

        FacebookSdk.sdkInitialize(this.getApplicationContext());

        requestWindowFeature(Window.FEATURE_NO_TITLE); //hide title
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN); //hide top bar

        setContentView(R.layout.help);

        //Set listener to button
        btn_menu = (Button) findViewById(R.id.btn_menu);
        btn_menu.setOnClickListener(this);


        // Week 14 -Init for FB
        //FacebookSdk.sdkInitialize(this.getApplicationContext());
        //setContentView(R.layout.mainmenu);

        // Week 14 - Define FB button
        btn_fbLogin = (LoginButton) findViewById(R.id.fb_login_button);
        //btn_fbLogin.setOnClickListener(this);
        ((LoginButton) btn_fbLogin).setReadPermissions(Arrays.asList((EMAIL)));

        LoginManager.getInstance().logInWithReadPermissions(this,Arrays.asList("public_profile","email"));

        profile_pic = (ProfilePictureView) findViewById(R.id.picture);
        callbackManager = CallbackManager.Factory.create();

        shareButton = (ShareButton)findViewById(R.id.fb_share_button);
        shareDialog = new ShareDialog(this);

        AccessTokenTracker accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {

                if (currentAccessToken == null){
                    profile_pic.setProfileId("");
                }
                else{
                    Profile temp;
                    temp = Profile.getCurrentProfile();
                    if (temp != null) {
                        profile_pic.setProfileId(temp.getCurrentProfile().getId());
                    }
                    //profile_pic.setProfileId(Profile.getCurrentProfile().getId());
                }
            }
        };
        accessTokenTracker.startTracking();

        loginManager = LoginManager.getInstance();
        //loginManager.logInWithPublishPermissions(this, PERMISSIONS);

        loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {



                Profile temp;
                temp = Profile.getCurrentProfile();
                if (temp != null) {
                    profile_pic.setProfileId(temp.getCurrentProfile().getId());
                }

               // profile_pic.setProfileId(Profile.getCurrentProfile().getId());

                shareScore();

                AccessToken accessToken = AccessToken.getCurrentAccessToken();
                loginResult.getAccessToken().getUserId();
            }

            @Override
            public void onCancel() {
                System.out.println("Login attempt canceled.");
            }

            @Override
            public void onError(FacebookException error) {
                System.out.println("Login attempt failed.");
            }
        });
    }

    @Override // will happen if there is an tap of the button on the screen/view
    public void onClick(View v)
    {
        Intent intent = new Intent(); //Intent = action to be performed

        //Intent is an object that provides runtime binding (e.g 2 or more activities occurring at one time)
        if (v == btn_menu)
        {
            intent.setClass(this, Mainmenu.class);
            startActivity(intent);
        }

    }

    public void shareScore() {
        Bitmap image = BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);

        SharePhoto photo = new SharePhoto.Builder().setBitmap(image).setCaption("Thank you for playing. Your Score is 1.").build();

        SharePhotoContent content = new SharePhotoContent.Builder().addPhoto(photo).build();

        ShareLinkContent linkContent = new ShareLinkContent.Builder()
                .setContentUrl(Uri.parse("http://developers.facebook.com/android"))
                .build();
        shareDialog.show(linkContent);

        shareButton.setShareContent(content);
        shareDialog.show(this, content);

        ShareApi.share(content, null);
    }




    public void CheckLogin() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        boolean isLoggedIn = accessToken != null && !accessToken.isExpired();

        if (isLoggedIn == true)
        {
            System.out.println("God");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        callbackManager.onActivityResult(requestCode,resultCode,data);
    }

    protected void onPause(){
        super.onPause();
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
    }
}
