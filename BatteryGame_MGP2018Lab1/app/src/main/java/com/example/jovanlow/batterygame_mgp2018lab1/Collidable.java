package com.example.jovanlow.batterygame_mgp2018lab1;

public interface Collidable {

    String GetType();

    float GetPosX();
    float GetPosY();
    float GetPosZ();

    void OnHit(Collidable _Other);

}
