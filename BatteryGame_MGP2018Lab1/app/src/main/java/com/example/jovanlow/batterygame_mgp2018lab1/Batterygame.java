package com.example.jovanlow.batterygame_mgp2018lab1;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.Typeface;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.VibrationEffect;
import android.util.DisplayMetrics;
import android.util.Pair;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Locale;
import java.util.Queue;
import java.util.Random;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

import android.os.Vibrator;

//Your game will be here babyy
public class Batterygame extends Activity implements View.OnTouchListener, SensorEventListener {

    //Create a singleton
    public final static Batterygame Instance = new Batterygame();

    public enum GAME_RESULT
    {
        GAME_ONGOING,
        GAME_WIN,
        GAME_LOSE
    }
    GAME_RESULT gameresult;
    float finishscreen_offset = 0;
    double gameCountdownToEnd = 6.0;
    boolean NewRecord = false;
    String finishedDialogue = "";
    Bitmap finishBgBox;

    //Game stuff, etc.
    private Bitmap bmp, playerProjGhostbmp, playerProjBoomerangbmp, playerProjJJbmp, bar, swapbutton, barMeter, timeFull, timeMid, timeLow; //for loading of images
    private Bitmap fossilcar_Normal, fossilcar_Special, fossilcar_Capturable, fossilcar_Dead;
    SampleEntity player;

    private Bitmap storedSwipebutton, storedTapbutton;

    private Bitmap rc_wasteland, rc_smoke0, rc_smoke1;

    private Bitmap ERP;
    private Sprite ERP_TITLE;
    private Bitmap hpbar;
    private Matrix temp;

    private Bitmap miscbox;

    boolean GameRunning = false;

    //decorative
    Mesh sea, cloudUpper, cloudLower, sky, roads, rock0, rock1, electricCar, playerproj, debugrect;

    float offset = 0.0f;

    int screenWidth, screenHeight;
    float xPos = 0.0f, yPos = 0.0f;
    float xCloud = 0.0f, yCloud = 0.0f;

    static final float groundLevel = 700.0f;
    static final float roadUpperDist = -200.0f;
    static final float roadLowerDist = 0.0f;

    Region roadTouch, roadTouchLeft, roadTouchRight, swapWeapons, swapControls;
    float playerRoadTouchboxOffset = 0;

    private Mesh captureButtonMesh, specialButtonMesh;
    Region captureButton, specialButton;
    float captureCharge = 100, captureChargeMax = 100;

//ASSIGNMENT 2 BELOW HERE !! (NOTE: YOU MUST ALSO INITIALIZE IN INIT())
    Region pauseButton, pause_SettingsRegion, pause_ExitRegion; //bring up alert for exiting maybe?
    Region settings_SkipDialogueToggleRegion, settings_MusicSlider, settings_SoundSlider, settings_Back;
    Bitmap pausebuttonbmp, otherButtonbmp, sliderbmp;
    boolean gamePaused = false, InSettings = false, skipDialogue = false;

//ASSIGNMENT 2 ABOVE HERE

    boolean swipeControls = false;

    double timeLeft = 300, timeLeftMax = 300;

    double playerSpecialMoveTime = 0.0, specialmoveeffectCD = 0.0;

    LinkedList<LevelPacket> levelInfo = new LinkedList<>();
    LinkedList<EnemyData> enemyqueue = new LinkedList<>();

    String CurrentDialogue = "";
    int CurrentDialogueDisplayLength = 0;
    double NextDialogueCharacterCooldown = 0.0;
    double CurrentDialogueRemainingTime = 0.0;

    Bitmap feedback_bmp, feedback_bmp_displayed;
    double feedback_lifetime = 1.0;
    float feedback_width = 0;
    float feedback_height = 0;
    boolean feedbackstart = false;
    int feedbackX = 0;
    int feedbackY = 0;

    //////////////////Typeface text;

    // accelerometer stuff
    private SensorManager sensorManager;
    private Sensor accelerometer;

    private float[ ] values = {0,0,0};
    private long lastTime = System.currentTimeMillis();
    private Bitmap ball;
    private float bX, bY;

    //MediaPlayer MusicControl;

    //private TextView text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//
//        //create LInearLayout
//        LinearLayout myLInearLayout = new LinearLayout(getApplicationContext());
//
//        //add LayoutParams
//        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//        myLInearLayout.setOrientation(LinearLayout.HORIZONTAL);
//
//        //set Content View
//        setContentView(myLInearLayout);
//
//        gamecontext = Batterygame.this;
//
//        //add textView
//        text = new TextView(this);
//        text.setText("CAPTURE");
//        text.setId(0);
//        text.setLayoutParams(params);
//        myLInearLayout.addView(text);
//
//
//        MusicControl = MediaPlayer.create(this, R.raw.blackorwhite_test); //maybe use hashmap for diff soundeffects with more mediaplayers?
//        {//'99' is current volume level
//            MusicControl.start();
//            float volume = (float) (1 - (Math.log(100 - 99) / Math.log(100)));
//            MusicControl.setVolume(volume, volume);
//        }
//
        System.out.println("ONCREATE");
    }
    TextView text;
    Typeface face;

    public void SetGameConclusion(GAME_RESULT _result)
    {
        if (gameresult != GAME_RESULT.GAME_ONGOING) return;

        gameresult = _result;
        NewRecord = RecordScore(MusicController.Instance.CurrentGameScore);
        finishscreen_offset = -screenWidth;

        if (_result == GAME_RESULT.GAME_WIN)
        {
            if (MusicController.Instance.MusicControl != null) {
                MusicController.Instance.MusicControl.reset();
                MusicController.Instance.MusicControl.release();
                MusicController.Instance.MusicControl = null;
            }
            MusicController.Instance.MusicControl = MediaPlayer.create(MusicController.Instance.gamepageContext, R.raw.fragment); //maybe use hashmap for diff soundeffects with more mediaplayers?
            {//'99' is current volume level
                MusicController.Instance.MusicControl.start();
                float volume = (float) (1 - (Math.log(100 - MusicController.Instance.MusicVolume) / Math.log(100)));
                MusicController.Instance.MusicControl.setVolume(volume, volume);
            }
        }
        else
        {
            MusicController.Instance.Stop();
        }

    }

    public boolean RecordScore(float _score)
    {
        for (int i = 0; i < 5; ++i)
        {
            if (_score > MusicController.Instance.LeaderBoard[i])
            {
                for (int j = 5 - 1 - 1; j >= i; --j)
                {
                    MusicController.Instance.LeaderBoard[j + 1] = MusicController.Instance.LeaderBoard[j];
                }

                MusicController.Instance.LeaderBoard[i] = _score;

                return true;
            }
        }
        return false;
    }

    //Init
    public void Init(SurfaceView _view)
    {

        //onCreate(new Bundle());
        System.out.println("INIT");

        gameresult = GAME_RESULT.GAME_ONGOING;
        feedback_lifetime = 1.0;
        feedback_width = 0;
        feedback_height = 0;
        feedbackstart = false;
        feedbackX = 0;
        feedbackY = 0;
        gameCountdownToEnd = 6.0;
        NewRecord = false;
        finishedDialogue = "";

        MusicController.Instance.CurrentGameScore = 0.0f;

        GameRunning = true;
        offset = 0.0f;
        xPos = 0.0f;
        yPos = 0.0f;
        xCloud = 0.0f;
        yCloud = 0.0f;
        playerRoadTouchboxOffset = 0;
        captureCharge = 100;
        captureChargeMax = 100;
        gamePaused = false;
        InSettings = false;
        skipDialogue = false;
        swipeControls = false;
        timeLeft = 300;
        timeLeftMax = 300;
        playerSpecialMoveTime = 0.0;
        specialmoveeffectCD = 0.0;
        levelInfo.clear();
        enemyqueue.clear();
        CurrentDialogue = "";
        CurrentDialogueDisplayLength = 0;
        NextDialogueCharacterCooldown = 0.0;
        CurrentDialogueRemainingTime = 0.0;
        Time.Instance.gameSpeed = 1;
        Time.Instance.entitySpeed = 1;


        levelInfo = (LinkedList<LevelPacket>) Level.Instance.Level1.clone(); //placeholder, gets replaced after StartLevel is called

//System.out.println(levelInfo.peek().datatype);


//        LinearLayout myLInearLayout = new LinearLayout(MusicController.Instance.gamepageContext);
//
//        //add LayoutParams
//        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
//        myLInearLayout.setOrientation(LinearLayout.HORIZONTAL);
//
//        //set Content View
//        setContentView(myLInearLayout);
//        //add textView
//        text = new TextView(MusicController.Instance.gamepageContext);
//        text.setText("CAPTURE");
//        text.setId(0);
//        text.setLayoutParams(params);
//        myLInearLayout.addView(text);



//        MusicControl = MediaPlayer.create(gamecontext, R.raw.blackorwhite_test);
//        //if (MusicControl != null)
//        {
//            MusicControl.start();
//        }



        //text = (TextView)(_view);

        timeLeft = timeLeftMax;
        //retreive info of your surfaceview or device size view
        DisplayMetrics metrics = _view.getResources().getDisplayMetrics();
        screenWidth = metrics.widthPixels;
        screenHeight = metrics.heightPixels;
        Matrix matrix = new Matrix();
        Matrix temp = new Matrix();

        _view.setOnTouchListener(this);

        //BOTTOM ROAD CURRENTLY
        roadTouch = new Region(0, (int)(metrics.heightPixels * 0.75), metrics.widthPixels,  metrics.heightPixels);
        //roadTouch.translate(0, (int)(screenHeight * 0.75f));

        roadTouchLeft = new Region(0, (int)(metrics.heightPixels * 0.5) , metrics.widthPixels / 2,  (int)(metrics.heightPixels * 0.75));
        //roadTouchLeft.translate(-100, 100);

        roadTouchRight = new Region(metrics.widthPixels / 2, (int)(metrics.heightPixels * 0.5), metrics.widthPixels, (int)(metrics.heightPixels * 0.75));
       // roadTouchRight.translate(100, 100);

        captureButton = new Region(0, (int)(metrics.heightPixels * 0.15), (int)(metrics.widthPixels * 0.25), (int)(metrics.heightPixels * 0.3));
        specialButton = new Region((int)(metrics.widthPixels * 0.50), 0, (int)(metrics.widthPixels * 0.75), (int)(metrics.heightPixels * 0.15));

        pauseButton = new Region((int)(metrics.widthPixels * 0.30), 0, (int)(metrics.widthPixels * 0.30 + metrics.heightPixels * 0.2), (int)(metrics.heightPixels * 0.2));
        pause_SettingsRegion = new Region((int)(metrics.widthPixels * 0.20), (int)(metrics.heightPixels * 0.50), (int)(metrics.widthPixels * 0.50), (int)(metrics.heightPixels * 0.70));
        pause_ExitRegion = new Region((int)(metrics.widthPixels * 0.20), (int)(metrics.heightPixels * 0.75), (int)(metrics.widthPixels * 0.50), (int)(metrics.heightPixels * 0.95));
        settings_SkipDialogueToggleRegion = new Region((int)(metrics.widthPixels * 0.20), (int)(metrics.heightPixels * 0.25), (int)(metrics.widthPixels * 0.50), (int)(metrics.heightPixels * 0.45));
        settings_Back = new Region((int)(metrics.widthPixels * 0.20), (int)(metrics.heightPixels * 0.75), (int)(metrics.widthPixels * 0.50), (int)(metrics.heightPixels * 0.95));
        settings_MusicSlider = new Region((int)(metrics.widthPixels * 0.17), (int)(metrics.heightPixels * 0.52), (int)(metrics.widthPixels * 0.23), (int)(metrics.heightPixels * 0.58));
        settings_SoundSlider = new Region((int)(metrics.widthPixels * 0.17), (int)(metrics.heightPixels * 0.62), (int)(metrics.widthPixels * 0.23), (int)(metrics.heightPixels * 0.68));


        swapWeapons = new Region((int)(metrics.widthPixels * 0.85), (int)(metrics.heightPixels * 0.2), (int)(metrics.widthPixels), (int)(metrics.heightPixels * 0.3));
        swapControls = new Region((int)(metrics.widthPixels * 0.85), (int)(metrics.heightPixels * 0.3), (int)(metrics.widthPixels), (int)(metrics.heightPixels * 0.4));


        bmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.bw_sea); //load images
        sea = new Mesh();
        sea.mesh = Bitmap.createScaledBitmap(bmp, screenWidth, screenHeight, true);
        sea.velX = 100.0f;

        bmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.resultsbox);
        finishBgBox = Bitmap.createScaledBitmap(bmp, screenWidth, screenHeight, true);

        bmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.bw_sky);
        sky = new Mesh();
        sky.mesh = Bitmap.createScaledBitmap(bmp, screenWidth, screenHeight, true);

        bmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.pause);
        pausebuttonbmp = Bitmap.createScaledBitmap(bmp, pauseButton.getBounds().width(),  pauseButton.getBounds().height(), true);

        bmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.pausebuttons);
        otherButtonbmp = Bitmap.createScaledBitmap(bmp, pause_SettingsRegion.getBounds().width(), pause_SettingsRegion.getBounds().height(), true);

        bmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.pausebuttons);
        sliderbmp = Bitmap.createScaledBitmap(bmp, settings_MusicSlider.getBounds().width(), settings_MusicSlider.getBounds().height(), true);

        bmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.box);
        miscbox = Bitmap.createScaledBitmap(bmp, (int)(screenWidth * 0.75), (int)(pause_SettingsRegion.getBounds().height() * 1.4), true);

        //IGNORE THIS BMP!!!
        bmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.bw_car);
        electricCar = new Mesh();
        matrix.postScale(0.5f, 0.5f);
        electricCar.mesh = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);
        electricCar.x = screenWidth / 2.4f;
        electricCar.y = 700;
        electricCar.z = -200;
        matrix.postScale(2, 2);

        bmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.bw_proj);
        playerproj = new Mesh();
        playerproj.mesh = bmp;

        bmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.bw_cloudlower);
        cloudLower = new Mesh();
        cloudLower.mesh = Bitmap.createScaledBitmap(bmp, screenWidth, screenHeight, true);
        cloudLower.velX = 48.0f;

        bmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.bw_cloudupper);
        cloudUpper = new Mesh();
        cloudUpper.mesh = Bitmap.createScaledBitmap(bmp, screenWidth, screenHeight, true);
        cloudUpper.velX = 40;

        bmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.rc_wasteland);
        rc_wasteland = Bitmap.createScaledBitmap(bmp, screenWidth, screenHeight, true);
        rc_smoke0 = BitmapFactory.decodeResource(_view.getResources(), R.drawable.rc_smoke0);
        rc_smoke1 = BitmapFactory.decodeResource(_view.getResources(), R.drawable.rc_smoke1);

        bmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.bw_roads);
        roads = new Mesh();
        roads.mesh = Bitmap.createScaledBitmap(bmp, screenWidth, screenHeight, true);
        roads.velX = 320;

        bmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.bw_rock0);
        rock0 = new Mesh();
        rock0.mesh = bmp;
        rock0.x = - 2 * screenWidth;
        rock0.y = 400;
        rock0.velX = 160;

        bmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.bw_rock1);
        rock1 = new Mesh();
        rock1.mesh = bmp;
        rock1.x = - 2 * screenWidth;
        rock1.y = 40;
        rock1.velX = 240;

        //WARNING : MUST FIX FOR SPRITE
        player = SampleEntity.Create();
        matrix.postScale(0.5f, 0.5f);
        if (MusicController.Instance.cartype.compareTo("Electric Car") == 0)
            bmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.bw_car_normal);
        else if (MusicController.Instance.cartype.compareTo("Electric Bus") == 0)
            bmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.bw_bus_normal);
        player.SetSpriteSheetNormal(Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true), 2, 3, 12);
        player.spritesheet = player.normal_spritesheet;
        player.xPos = screenWidth / 2.4f;
        player.yPos = groundLevel;
        player.zPos = roadUpperDist;
        if (MusicController.Instance.cartype.compareTo("Electric Car") == 0)
            player.maxhp = 10;
        else if (MusicController.Instance.cartype.compareTo("Electric Bus") == 0)
            player.maxhp = 20;
        player.hp = player.maxhp;
        player.type = SampleEntity.Type.ElectricCar;
        player.canSpecial = true;
        if (MusicController.Instance.cartype.compareTo("Electric Car") == 0)
            player.specialChargeMax = 100;
        else if (MusicController.Instance.cartype.compareTo("Electric Bus") == 0)
            player.specialChargeMax = 200;
        player.specialCharge = player.specialChargeMax;
        player.Weapon = MusicController.Instance.CurrentWeapon;
        matrix.postScale(2, 2);

        fossilcar_Normal = BitmapFactory.decodeResource(_view.getResources(), R.drawable.bw_fossilcar_normal);
        fossilcar_Special = BitmapFactory.decodeResource(_view.getResources(), R.drawable.bw_fossilcar_special);
        fossilcar_Capturable = BitmapFactory.decodeResource(_view.getResources(), R.drawable.bw_fossilcar_capturable);
        fossilcar_Dead = BitmapFactory.decodeResource(_view.getResources(), R.drawable.bw_fossilcar_dead);

//        SampleEntity enemy = SampleEntity.Create();
//        matrix.postScale(0.5f, 0.5f);
//        enemy.SetSpriteSheetNormal(Bitmap.createBitmap(fossilcar_Normal, 0, 0, fossilcar_Normal.getWidth(), fossilcar_Normal.getHeight(), matrix, true), 2, 3, 12);
//        enemy.spritesheet = enemy.normal_spritesheet;
//        enemy.SetSpriteSheetCapture(Bitmap.createBitmap(fossilcar_Capturable, 0, 0, fossilcar_Capturable.getWidth(), fossilcar_Capturable.getHeight(), matrix, true), 2, 3, 12);
//        enemy.SetSpriteSheetDead(Bitmap.createBitmap(fossilcar_Dead, 0, 0, fossilcar_Dead.getWidth(), fossilcar_Dead.getHeight(), matrix, true), 2, 3, 12);
//        enemy.SetSpriteSheetAttackSpecial(Bitmap.createBitmap(fossilcar_Special, 0, 0, fossilcar_Special.getWidth(), fossilcar_Special.getHeight(), matrix, true), 2, 3, 12);
//        enemy.xPos = 200;
//        enemy.yPos = groundLevel;
//        enemy.zPos = roadLowerDist;
//        enemy.XSpeed = 20;
//        enemy.maxhp = 2;
//        enemy.hp = enemy.maxhp;
//        enemy.type = SampleEntity.Type.FossilCar;
//        enemy.canCapture = true;
//        enemy.captureChance = 50;
//        enemy.canSpecial = true;
//        matrix.postScale(2, 2);

        playerProjGhostbmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.bw_proj);
        playerProjBoomerangbmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.bw_proj_boomerang_spritesheet);
        playerProjJJbmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.bw_proj_jumpingjack);

        bmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.bw_special);
        ERP = Bitmap.createScaledBitmap(bmp, screenWidth, screenHeight, true);

        bmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.bw_specialtext);
        ERP_TITLE = new Sprite(Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true), 4, 3, 24);

        bmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.bar);
        bar = Bitmap.createScaledBitmap(bmp, captureButton.getBounds().width(),  captureButton.getBounds().height(), true);

        bmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.button_tap);
        swapbutton = Bitmap.createScaledBitmap(bmp, swapControls.getBounds().width(),  swapControls.getBounds().height(), true);

        storedTapbutton = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(_view.getResources(), R.drawable.button_tap), swapControls.getBounds().width(),  swapControls.getBounds().height(), true);
        storedSwipebutton = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(_view.getResources(), R.drawable.button_swipe), swapControls.getBounds().width(),  swapControls.getBounds().height(), true);;

        bmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.bar_meter);
        barMeter = Bitmap.createScaledBitmap(bmp, captureButton.getBounds().width(),  captureButton.getBounds().height(), true);
        //barMeter = bmp;

        timeFull = BitmapFactory.decodeResource(_view.getResources(), R.drawable.timefull);
        timeMid = BitmapFactory.decodeResource(_view.getResources(), R.drawable.timemid);
        timeLow = BitmapFactory.decodeResource(_view.getResources(), R.drawable.timelow);

        bmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.bar_meter);

        temp.postScale(1/player.maxhp,0.5f);
        hpbar = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), temp, true);
        temp.postScale(player.maxhp, 1/0.5f);

        bmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.button);
        debugrect = new Mesh();
        debugrect.mesh = bmp;



        bmp = BitmapFactory.decodeResource(_view.getResources(), R.drawable.yellowcircle);
        feedback_height = bmp.getHeight();
        feedback_width = bmp.getWidth();
        feedback_bmp = bmp;
        feedback_bmp_displayed = Bitmap.createScaledBitmap(feedback_bmp, /*0, 0, */ (int)feedback_width , (int)feedback_height /*, matrix*/, false);


        ball = BitmapFactory.decodeResource(_view.getResources(), R.drawable.ball);

        bX = screenWidth / 2.0f;
        bY = screenHeight / 2.0f;
    }


    //Update
    public void Update(float dt)
    {


        if (CurrentDialogueDisplayLength < CurrentDialogue.length()) {
            NextDialogueCharacterCooldown -= dt;
            if (NextDialogueCharacterCooldown <= 0.0) {
                int amountofchartemp = (int)(-NextDialogueCharacterCooldown / 0.05) + 1;
                ++CurrentDialogueDisplayLength;
                NextDialogueCharacterCooldown += 0.05 * amountofchartemp;
            }
        }

        SensorMove();

        //face = Typeface.createFromAsset(MusicController.Instance.gamepageContext.getAssets(), "gemcut.otf");
        //text.setTypeface(face);

        if (player.hp <= 0)
        {
            //Time.Instance.gameSpeed = 0;

            //Intent intent = new Intent(Batterygame.this, Mainmenu.class);
            //startActivity(intent);

            SetGameConclusion(GAME_RESULT.GAME_LOSE);

        }

        if (playerSpecialMoveTime > 0) ERP_TITLE.Update(dt);

        if (playerSpecialMoveTime > 0)
        {
            playerSpecialMoveTime -= dt;
            specialmoveeffectCD -= dt;
            if (specialmoveeffectCD <= 0 && playerSpecialMoveTime > 0)
            {
                //create the effect
                SampleEntity proj = SampleEntity.Create();
                Matrix matrix = new Matrix();
                proj.SetSpriteSheetNormal(Bitmap.createBitmap(ERP, 0, 0, ERP.getWidth(), ERP.getHeight(), matrix, true), 1, 1, 12);
                proj.spritesheet = proj.normal_spritesheet;
                proj.xPos = (float)(((1.5 - playerSpecialMoveTime) / 1.5) * screenWidth);
                proj.yPos = screenHeight/2;
                proj.existTime = 0.01;
                proj.type = SampleEntity.Type.Effect;

                specialmoveeffectCD = 0.05;
            }

            if (playerSpecialMoveTime <= 0)
            {
                EntityManager.Instance.DamageAllExceptPlayer(10);
                Time.Instance.entitySpeed = 1;
            }
        }

        timeLeft -= dt * Time.Instance.entitySpeed;
        offset += dt;

        if (captureCharge < captureChargeMax)
        {
            captureCharge += 40 * dt;
        }
        if (captureCharge > captureChargeMax)
        {
            captureCharge = captureChargeMax;
        }

        cloudUpper.x -= cloudUpper.velX * dt * Time.Instance.entitySpeed;
        if (cloudUpper.x < - screenWidth)
        {
            cloudUpper.x = 0;
        }

        cloudLower.x -= cloudLower.velX * dt * Time.Instance.entitySpeed;
        if (cloudLower.x < - screenWidth)
        {
            cloudLower.x = 0;
        }

        roads.x -= roads.velX * dt * Time.Instance.entitySpeed;
        if (roads.x < - screenWidth)
        {
            roads.x = 0;
        }

        sea.x -= sea.velX * dt * Time.Instance.entitySpeed;
        if (sea.x < - screenWidth)
        {
            sea.x = 0;
        }

        if (rock0.x >= - 2 * screenWidth)
        {
            rock0.x -= rock0.velX * dt * Time.Instance.entitySpeed;
        }
        else
        {
            //int rand = new Random().nextInt(100);
            //if (rand < 3)
            {
                rock0.x = 2 * screenWidth;
                rock0.y = new Random().nextFloat() * 80 + 320;
            }
        }

        CurrentDialogueRemainingTime -= dt;

        if (levelInfo.size() > 0 && gameresult == GAME_RESULT.GAME_ONGOING)
        {
            if (LevelPacketCondition(levelInfo.peek().condition)) //next time, replace with levelpacket condition
            {
                if (levelInfo.peek().datatype == Level.DataType.ENEMY)
                {
                    enemyqueue = levelInfo.peek().enemylist;
                    levelInfo.pop();
                }
                else if (levelInfo.peek().datatype == Level.DataType.MUSIC)
                {
                    if (MusicController.Instance.MusicControl != null) {
                        MusicController.Instance.MusicControl.reset();
                        MusicController.Instance.MusicControl.release();
                        MusicController.Instance.MusicControl = null;
                    }
                    MusicController.Instance.MusicControl = MediaPlayer.create(MusicController.Instance.gamepageContext, levelInfo.peek().musictrack); //maybe use hashmap for diff soundeffects with more mediaplayers?
                    {//'99' is current volume level
                        MusicController.Instance.MusicControl.start();
                        float volume = (float) (1 - (Math.log(100 - MusicController.Instance.MusicVolume) / Math.log(100)));
                        MusicController.Instance.MusicControl.setVolume(volume, volume);
                    }

                    levelInfo.pop();
                }
                else if (levelInfo.peek().datatype == Level.DataType.DIALOGUE)
                {
                    if (!skipDialogue) {
                        CurrentDialogue = levelInfo.peek().dialogue;
                        CurrentDialogueDisplayLength = 0;
                        NextDialogueCharacterCooldown = 0.0;
                        CurrentDialogueRemainingTime = levelInfo.peek().duration;
                    }
                    levelInfo.pop();
                }
                else if (levelInfo.peek().datatype == Level.DataType.WINGAME)
                {
                    finishedDialogue = levelInfo.peek().dialogue;
                    SetGameConclusion(GAME_RESULT.GAME_WIN);
                }
                else if (levelInfo.peek().datatype == Level.DataType.LOSEGAME)
                {
                    SetGameConclusion(GAME_RESULT.GAME_LOSE);
                }
            }
        }

        if (enemyqueue.size() > 0)
        {
            for (EnemyData currenemy : enemyqueue) {
                currenemy.timeToSpawn -= dt * Time.Instance.entitySpeed;
             }
             while(enemyqueue.peek() != null) {
                 if (enemyqueue.peek().timeToSpawn <= 0) {
                     //spawn enemy

                     Matrix matrix = new Matrix();
                     SampleEntity enemy = SampleEntity.Create();

                     switch(enemyqueue.peek().spawn)
                     {
                         case TOPLEFT:
                             matrix.postScale(0.5f, 0.5f);
                             enemy.xPos = -400;
                             enemy.zPos = roadUpperDist;
                             enemy.XSpeed = 100;
                             break;
                         case TOPRIGHT:
                             matrix.postScale(-0.5f, 0.5f);
                             enemy.xPos = screenWidth + 400;
                             enemy.zPos = roadUpperDist;
                             enemy.XSpeed = -100;
                             break;
                         case BOTTOMLEFT:
                             matrix.postScale(0.5f, 0.5f);
                             enemy.xPos = -400;
                             enemy.zPos = roadLowerDist;
                             enemy.XSpeed = 100;
                             break;
                         case BOTTOMRIGHT:
                             matrix.postScale(-0.5f, 0.5f);
                             enemy.xPos = screenWidth + 400;
                             enemy.zPos = roadLowerDist;
                             enemy.XSpeed = -100;
                             break;
                         default:break;
                     }

                     enemy.SetSpriteSheetNormal(Bitmap.createBitmap(fossilcar_Normal, 0, 0, fossilcar_Normal.getWidth(), fossilcar_Normal.getHeight(), matrix, true), 2, 3, 12);
                     enemy.spritesheet = enemy.normal_spritesheet;
                     enemy.SetSpriteSheetCapture(Bitmap.createBitmap(fossilcar_Capturable, 0, 0, fossilcar_Capturable.getWidth(), fossilcar_Capturable.getHeight(), matrix, true), 2, 3, 12);
                     enemy.SetSpriteSheetDead(Bitmap.createBitmap(fossilcar_Dead, 0, 0, fossilcar_Dead.getWidth(), fossilcar_Dead.getHeight(), matrix, true), 2, 3, 12);
                     enemy.SetSpriteSheetAttackSpecial(Bitmap.createBitmap(fossilcar_Special, 0, 0, fossilcar_Special.getWidth(), fossilcar_Special.getHeight(), matrix, true), 2, 3, 12);

                     enemy.yPos = groundLevel;
                     enemy.maxhp = enemyqueue.peek().hp;
                     enemy.hp = enemy.maxhp;
                     if (enemyqueue.peek().enemytype == SampleEntity.Type.FossilCar)
                     {
                         enemy.type = SampleEntity.Type.FossilCar;
                         enemy.canCapture = true;
                         enemy.captureChance = 75;
                     }
                     enemy.damage = enemyqueue.peek().damage;
                     enemy.canSpecial = enemyqueue.peek().canSpecial;

                     matrix.postScale(2, 2);



                     enemyqueue.remove();
                 }
                 else
                 {
                     break;
                 }
             }
        }

        // Test for ball
        if (player.state == SampleEntity.State.Normal && swipeControls) {
            if (Math.abs(player.zPos - roadUpperDist) < 0.01) {
                if (values[0] > 2) {
                    player.state = SampleEntity.State.Jumping;
                    player.YSpeed = -400; //does not change depending where
                    player.ZSpeed = 400;//changes depending where
                    player.YAccel = 1600;
                    player.stateTime = 0.5;
                    playerRoadTouchboxOffset = (int) (screenHeight * 0.25);
                }
            } else if (Math.abs(player.zPos - roadLowerDist) < 0.01) {
                if (values[0] < -2) {
                    player.state = SampleEntity.State.Jumping;
                    player.YSpeed = -400; //does not change depending where
                    player.ZSpeed = -400;//changes depending where
                    player.YAccel = 1600;
                    player.stateTime = 0.5;
                    playerRoadTouchboxOffset = 0;
                }
            }
        }

        if (sensorManager == null)
        {
            sensorManager = (SensorManager) MusicController.Instance.gamepageContext.getApplicationContext().getSystemService(MusicController.Instance.gamepageContext.SENSOR_SERVICE);
            accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        }

    }

    //Render
    public void Render(Canvas _canvas)
    {
        //int currOffset = (int)(offset * 100.0f);
        //_canvas.drawBitmap(bmp, (10 + currOffset)% 500, 10, null);

        //the trick to looping is to render twice, in this case this is the looped bg
       // _canvas.drawBitmap(bmpSea, xPos, yPos, null);
        //_canvas.drawBitmap(bmpSea, xPos+ screenWidth,yPos,null);


        _canvas.drawBitmap(sky.mesh, 0, 0, null);

        _canvas.drawBitmap(sea.mesh, sea.x, sea.y, null);
        _canvas.drawBitmap(sea.mesh, sea.x+ screenWidth,sea.y,null);

        _canvas.drawBitmap(cloudLower.mesh, cloudLower.x, cloudLower.y, null);
        _canvas.drawBitmap(cloudLower.mesh, cloudLower.x+ screenWidth,cloudLower.y,null);

        _canvas.drawBitmap(cloudUpper.mesh, cloudUpper.x, cloudUpper.y, null);
        _canvas.drawBitmap(cloudUpper.mesh, cloudUpper.x + screenWidth,cloudUpper.y,null);

        _canvas.drawBitmap(rock0.mesh, rock0.x, rock0.y, null);

        _canvas.drawBitmap(roads.mesh, roads.x, roads.y, null);
        _canvas.drawBitmap(roads.mesh, roads.x+ screenWidth,roads.y,null);

        //_canvas.drawBitmap(ball, bX - ball.getWidth()/2, bY - ball.getHeight()/2, null);

        //        paint = new Paint();
//        paint.setColor(Color.BLACK);
//        paint.setStrokeWidth(8);
//        paint.setStyle(Paint.Style.STROKE);
//        Rect hitbox;
//        hitbox = captureButton.getBounds();
//        _canvas.drawRect(hitbox.left, hitbox.top, hitbox.right, hitbox.bottom, paint);
//
//        hitbox = specialButton.getBounds();
//        _canvas.drawRect(hitbox.left, hitbox.top, hitbox.right, hitbox.bottom, paint);


        //_canvas.drawRect(roadTouchLeft.getBounds().left, roadTouchLeft.getBounds().top + playerRoadTouchboxOffset, roadTouchLeft.getBounds().right, roadTouchLeft.getBounds().bottom + playerRoadTouchboxOffset, paint);
        //_canvas.drawRect(roadTouchRight.getBounds().left, roadTouchRight.getBounds().top + playerRoadTouchboxOffset, roadTouchRight.getBounds().right, roadTouchRight.getBounds().bottom + playerRoadTouchboxOffset, paint);

        //_canvas.drawBitmap(debugrect.mesh, roadTouchLeft.getBounds().centerX(), roadTouchLeft.getBounds().centerY() + player.zPos - roadUpperDist, null);

        //_canvas.drawBitmap(electricCar.mesh, electricCar.x+ electricCar.z * 0.2f, electricCar.y + electricCar.z, null);

    }

    public void RenderUI(Canvas _canvas)
    {
        Rect src, dst;
        src = new Rect(0, 0, (int)(barMeter.getWidth() * (captureCharge/captureChargeMax)), barMeter.getHeight());
        dst = new Rect(captureButton.getBounds().left, captureButton.getBounds().top, captureButton.getBounds().left + (int)(barMeter.getWidth() * (captureCharge/captureChargeMax)), captureButton.getBounds().bottom);
        _canvas.drawBitmap(barMeter, src, dst, null);
        src = new Rect(0, 0, (int)(barMeter.getWidth() * (player.specialCharge / player.specialChargeMax)), barMeter.getHeight());
        dst = new Rect(specialButton.getBounds().left, specialButton.getBounds().top, specialButton.getBounds().left + (int)(barMeter.getWidth() * (player.specialCharge/player.specialChargeMax)), specialButton.getBounds().bottom);
        _canvas.drawBitmap(barMeter, src, dst, null);

        _canvas.drawBitmap(bar, captureButton.getBounds().left, captureButton.getBounds().top, null);
        _canvas.drawBitmap(bar, specialButton.getBounds().left, specialButton.getBounds().top, null);
        _canvas.drawBitmap(pausebuttonbmp, pauseButton.getBounds().left, pauseButton.getBounds().top, null);

        //_canvas.drawBitmap(swapbutton, swapWeapons.getBounds().left, swapWeapons.getBounds().top, null);
        _canvas.drawBitmap(swapbutton, swapControls.getBounds().left, swapControls.getBounds().top, null);

        Paint pausemenupaint = new Paint();

        if (face == null)
            face = Typeface.createFromAsset(MusicController.Instance.gamepageContext.getAssets(), "fonts/gemcut.otf");

        if (gamePaused) {
            pausemenupaint.setTypeface(face);
            pausemenupaint.setARGB(255, 255, 255, 255);
            pausemenupaint.setStrokeWidth(100);
            pausemenupaint.setTextSize(144);
            _canvas.drawText("PAUSE!", (int)(screenWidth * 0.7), (int) (screenHeight * 0.5), pausemenupaint);
        }


        if (gamePaused && !InSettings)
        {
            _canvas.drawBitmap(otherButtonbmp, pause_SettingsRegion.getBounds().left, pause_SettingsRegion.getBounds().top, null);
            _canvas.drawBitmap(otherButtonbmp, pause_ExitRegion.getBounds().left, pause_ExitRegion.getBounds().top, null);

            pausemenupaint.setTypeface(face);
            pausemenupaint.setARGB(255, 255, 255, 255);
            pausemenupaint.setStrokeWidth(100);
            pausemenupaint.setTextSize(72);
            _canvas.drawText("Settings", (int)(pause_SettingsRegion.getBounds().left + 100), (int) (pause_SettingsRegion.getBounds().centerY()), pausemenupaint);
            _canvas.drawText("Exit", (int)(pause_ExitRegion.getBounds().left + 100), (int) (pause_ExitRegion.getBounds().centerY()), pausemenupaint);


        }
        else if (gamePaused && InSettings)
        {
            _canvas.drawBitmap(miscbox, screenWidth * 0.05f, settings_MusicSlider.getBounds().top - 100, null);

            _canvas.drawBitmap(sliderbmp, settings_MusicSlider.getBounds().left + (int)((MusicController.Instance.MusicVolume * 0.01) * 0.5 * screenWidth), settings_MusicSlider.getBounds().top, null);
            _canvas.drawBitmap(sliderbmp, settings_SoundSlider.getBounds().left + (int)((MusicController.Instance.SoundVolume * 0.01) * 0.5 * screenWidth), settings_SoundSlider.getBounds().top, null);

            _canvas.drawBitmap(otherButtonbmp, settings_SkipDialogueToggleRegion.getBounds().left, settings_SkipDialogueToggleRegion.getBounds().top, null);
            _canvas.drawBitmap(otherButtonbmp, settings_Back.getBounds().left, settings_Back.getBounds().top, null);


            pausemenupaint.setTypeface(face);
            pausemenupaint.setARGB(255, 255, 255, 255);
            pausemenupaint.setStrokeWidth(100);
            pausemenupaint.setTextSize(72);
            if (skipDialogue)
                _canvas.drawText("Un-Skip Dialogue", (int)(settings_SkipDialogueToggleRegion.getBounds().left + 100), (int) (settings_SkipDialogueToggleRegion.getBounds().centerY()), pausemenupaint);
            else
                _canvas.drawText("Skip Dialogue", (int)(settings_SkipDialogueToggleRegion.getBounds().left + 100), (int) (settings_SkipDialogueToggleRegion.getBounds().centerY()), pausemenupaint);
            _canvas.drawText("Back", (int)(settings_Back.getBounds().left + 100), (int) (settings_Back.getBounds().centerY()), pausemenupaint);



        }

        for (int i = 0; i < player.maxhp;i++)
        {
            if (i < player.hp) {

                _canvas.drawBitmap(hpbar, i * 10, 0, null);
            }
        }

        if (timeLeft <= timeLeftMax * 0.333) {
            _canvas.drawBitmap(timeLow, screenWidth - timeLow.getWidth(), 0, null);
        }
        else if (timeLeft <= timeLeftMax * 0.667) {
            _canvas.drawBitmap(timeMid, screenWidth - timeMid.getWidth(), 0, null);
        }
        else {
            _canvas.drawBitmap(timeFull, screenWidth - timeFull.getWidth(), 0, null);
        }

        if (playerSpecialMoveTime > 0) ERP_TITLE.Render(_canvas, screenWidth/2, 0, 0);

        Paint paint = new Paint();

        if (face == null)
            face = Typeface.createFromAsset(MusicController.Instance.gamepageContext.getAssets(), "fonts/gemcut.otf");

        if (CurrentDialogueRemainingTime > 0) {
            paint.setTypeface(face);
            paint.setARGB(255, 0, 120, 120);
            paint.setStrokeWidth(100);
            paint.setTextSize(72);
            _canvas.drawText(CurrentDialogue, 0, CurrentDialogueDisplayLength, 10, (int) (screenHeight * 0.4), paint);
        }
        paint.setTypeface(face);
        paint.setARGB(255, 0, 0, 0);
        paint.setStrokeWidth(100);
        paint.setTextSize(72);
        String currentTimeTemp = (int)(timeLeft/60) + ":" + (int)(timeLeft % 60);
        _canvas.drawText(currentTimeTemp, screenWidth - timeLow.getWidth() - 200, 100, paint);

        if (feedback_lifetime < 1.0) {
            //feedback_bmp_displayed.recycle();
            feedback_bmp_displayed = Bitmap.createScaledBitmap(feedback_bmp, /*0, 0, */ (int) feedback_width - (int) (feedback_lifetime * feedback_width), (int) feedback_height - (int) (feedback_lifetime * feedback_height)/*, matrix*/, false);
            _canvas.drawBitmap(feedback_bmp_displayed, feedbackX - (int) (0.5 * feedback_width * (1.0 - feedback_lifetime)), feedbackY - (int) (0.5 * feedback_height * (1.0 - feedback_lifetime)), null);
        }


        if (gameresult != GAME_RESULT.GAME_ONGOING) {

            _canvas.drawBitmap(finishBgBox,  (int)finishscreen_offset, 0, null);
            //result title
            if (gameresult == GAME_RESULT.GAME_WIN)
            {
                String conclusiontext = "VICTORY";
                paint.setTypeface(face);
                paint.setARGB(255, 0, 0, 0);
                paint.setStrokeWidth(100);
                paint.setTextSize(144);
                _canvas.drawText(conclusiontext, (int)(screenWidth * 0.25) + (int)finishscreen_offset, (int)(screenHeight * 0.25), paint);
            }
            else if (gameresult == GAME_RESULT.GAME_LOSE)
            {
                String conclusiontext = "LOSS";
                paint.setTypeface(face);
                paint.setARGB(255, 0, 0, 0);
                paint.setStrokeWidth(100);
                paint.setTextSize(144);
                _canvas.drawText(conclusiontext, (int)(screenWidth * 0.35) + (int)finishscreen_offset, (int)(screenHeight * 0.25), paint);
            }

            //Display Record Added
            if (NewRecord)
            {
                String recordtext = "SCORE RECORDED!";
                paint.setTypeface(face);
                paint.setARGB(255, 0, 0, 127);
                paint.setStrokeWidth(100);
                paint.setTextSize(80);
                _canvas.drawText(recordtext, (int)(screenWidth * 0.1) + (int)finishscreen_offset, (int)(screenHeight * 0.45), paint);
            }

            //Display score
            String scoretext = Float.toString(MusicController.Instance.CurrentGameScore);
            paint.setTypeface(face);
            paint.setARGB(255, 0, 0, 0);
            paint.setStrokeWidth(100);
            paint.setTextSize(72);
            _canvas.drawText(scoretext, (int)(screenWidth * 0.1) + (int)finishscreen_offset, (int)(screenHeight * 0.55), paint);

            //last words
            if (gameresult == GAME_RESULT.GAME_WIN)
            {
                String dialoguefinaltext = finishedDialogue;
                paint.setTypeface(face);
                paint.setARGB(255, 127, 0, 127);
                paint.setStrokeWidth(100);
                paint.setTextSize(50);
                _canvas.drawText(dialoguefinaltext, (int)(screenWidth * 0.1) + (int)finishscreen_offset, (int)(screenHeight * 0.65), paint);

            }

            //Display time before going back to main menu
            String timelefttext = Double.toString(Math.round(gameCountdownToEnd));
            paint.setTypeface(face);
            paint.setARGB(255, 0, 0, 0);
            paint.setStrokeWidth(100);
            paint.setTextSize(72);
            _canvas.drawText(timelefttext, (int)(screenWidth * 0.4) + (int)finishscreen_offset, (int)(screenHeight * 0.8), paint);


        }

    }


    public boolean onTouch(View v, MotionEvent m)
    {
        //System.out.println((int)m.getX() + " | " + (int)m.getY() + " | " + screenWidth + " | " + screenHeight);
        TouchManager.Instance.Update((int)m.getX(), (int)m.getY(), m.getAction());

        Rect hitbox;

        if(TouchManager.Instance.HasTapped()) {
            feedbackstart = true;
            FeedbackStart((int) m.getX(), (int) m.getY());
        }

        if (gameresult != GAME_RESULT.GAME_ONGOING) return true;

        if(TouchManager.Instance.HasTapped()) {

            hitbox = pauseButton.getBounds();
            if (Collision.AABBCollision(0, 0, 0, m.getX(), m.getY(), 0,
                    hitbox.left, hitbox.top, -0.2f, hitbox.right, hitbox.bottom, 0.2f, -0, -0, -0, 0, 0, 0)) {


//                LinkedList<String> textInfo = new LinkedList<>();
//                textInfo = readLine("level1.txt");
//                for (String currentline : textInfo)
//                {
//                    System.out.println(currentline);
//                }
//                textInfo.clear();

                if (gamePaused) {
                    Time.Instance.gameSpeed = 1;

                    gamePaused = false;
                    InSettings = false;
                }
                else if (!gamePaused) {
                    Time.Instance.gameSpeed = 0;

                    gamePaused = true;
                    InSettings = false;
                }


                return true;
            }
        }


        if (!gamePaused)
        {
        if(TouchManager.Instance.HasTapped())
        {

            //ROAD-JUMPING TAP-HITBOX FOR PLAYER

            if (playerSpecialMoveTime <= 0) {
                hitbox = captureButton.getBounds();
                if (Collision.AABBCollision(0, 0, 0, m.getX(), m.getY(), 0,
                        hitbox.left, hitbox.top, -0.2f, hitbox.right, hitbox.bottom, 0.2f, -0, -0, -0, 0, 0, 0)) {

                    if (captureCharge >= captureChargeMax) {
                        int amountcaptured = EntityManager.Instance.CaptureUnleash();
                        player.captured += amountcaptured;
                        player.specialCharge += amountcaptured * 40;
                        if (player.specialCharge >= player.specialChargeMax) {
                            player.specialCharge = player.specialChargeMax;
                        }
                        captureCharge = 0;

                    }
                    return true;
                }
                hitbox = specialButton.getBounds();
                if (Collision.AABBCollision(0, 0, 0, m.getX(), m.getY(), 0,
                        hitbox.left, hitbox.top, -0.2f, hitbox.right, hitbox.bottom, 0.2f, -0, -0, -0, 0, 0, 0)) {

                    if (player.specialCharge >= player.specialChargeMax) {
                        Time.Instance.entitySpeed = 0;
                        playerSpecialMoveTime = 1.5;
                        player.specialCharge = 0;

                        Vibrator vibrator = (Vibrator) MusicController.Instance.gamepageContext.getSystemService(Context.VIBRATOR_SERVICE);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            vibrator.vibrate(VibrationEffect.createOneShot(500, VibrationEffect.DEFAULT_AMPLITUDE));
                        } else {
                            //deprecated in API 26
                            vibrator.vibrate(500);
                        }

                    }
                    return true;
                }
//                hitbox = swapWeapons.getBounds();
//                if (Collision.AABBCollision(0, 0, 0, m.getX(), m.getY(), 0,
//                        hitbox.left, hitbox.top, -0.2f, hitbox.right, hitbox.bottom, 0.2f, -0, -0, -0, 0, 0, 0)) {
//
//                    if (player.Weapon == SampleEntity.ProjType.PlayerProj_FutureGhost)
//                        player.Weapon = SampleEntity.ProjType.PlayerProj_Boomerang;
//                    else if (player.Weapon == SampleEntity.ProjType.PlayerProj_Boomerang)
//                        player.Weapon = SampleEntity.ProjType.PlayerProj_FutureGhost;
//                    return true;
//                }
                hitbox = swapControls.getBounds();
                if (Collision.AABBCollision(0, 0, 0, m.getX(), m.getY(), 0,
                        hitbox.left, hitbox.top, -0.2f, hitbox.right, hitbox.bottom, 0.2f, -0, -0, -0, 0, 0, 0)) {

                    if (swipeControls == true) {
                        swipeControls = false;
                        swapbutton = storedTapbutton;
                    }
                    else if (swipeControls == false) {
                        swipeControls = true;
                        swapbutton = storedSwipebutton;
                    }
                    return true;
                }
            }



            if (!swipeControls) {
                hitbox = roadTouch.getBounds();
                if (Collision.AABBCollision(0, -playerRoadTouchboxOffset, 0, m.getX(), m.getY(), 0,
                        hitbox.left, hitbox.top, -0.2f, hitbox.right, hitbox.bottom, 0.2f, -0, -0, -0, 0, 0, 0)) {
                    //System.out.println(hitbox.left + " | " + hitbox.top + " | " + hitbox.right + " | " + hitbox.bottom);
                    if (player.state == SampleEntity.State.Normal) {
                        if (Math.abs(player.zPos - roadUpperDist) < 0.01) {
                            player.state = SampleEntity.State.Jumping;
                            player.YSpeed = -400; //does not change depending where
                            player.ZSpeed = 400;//changes depending where
                            player.YAccel = 1600;
                            player.stateTime = 0.5;
                            playerRoadTouchboxOffset = (int) (screenHeight * 0.25);
                        } else if (Math.abs(player.zPos - roadLowerDist) < 0.01) {
                            player.state = SampleEntity.State.Jumping;
                            player.YSpeed = -400; //does not change depending where
                            player.ZSpeed = -400;//changes depending where
                            player.YAccel = 1600;
                            player.stateTime = 0.5;
                            playerRoadTouchboxOffset = 0;
                        }
                    }
                    return true;
                }
                hitbox = roadTouchLeft.getBounds();

                if (Collision.AABBCollision(0, playerRoadTouchboxOffset, 0, m.getX(), m.getY(), 0,
                        hitbox.left, hitbox.top, -0.2f, hitbox.right, hitbox.bottom, 0.2f, -0, -0, -0, 0, 0, 0)) {
                    if (player.state == SampleEntity.State.Normal) {
                        if (player.Weapon == SampleEntity.ProjType.PlayerProj_FutureGhost) {
                            Matrix matrix = new Matrix();
                            SampleEntity proj = SampleEntity.Create();
                            matrix.postScale(-0.5f, 0.5f);
                            playerProjGhostbmp = Bitmap.createBitmap(playerproj.mesh);

                            proj.SetSpriteSheet(Bitmap.createBitmap(playerProjGhostbmp, 0, 0, playerProjGhostbmp.getWidth(), playerProjGhostbmp.getHeight(), matrix, true), 1, 1, 12);

                            proj.xPos = player.xPos;
                            proj.yPos = player.yPos;
                            proj.zPos = player.zPos;
                            proj.XSpeed = -1300;
                            proj.XAccel = 1300;
                            proj.existTime = 1.0;
                            proj.type = SampleEntity.Type.Projectile;
                            proj.projtype = SampleEntity.ProjType.PlayerProj_FutureGhost;
                            matrix.postScale(-2, 2);

                            player.state = SampleEntity.State.Attacking;
                            player.stateTime = 0.5;
                        }
                        else if (player.Weapon == SampleEntity.ProjType.PlayerProj_Boomerang)
                        {
                            Matrix matrix = new Matrix();
                            SampleEntity proj = SampleEntity.Create();
                            if (Math.abs(player.zPos - roadUpperDist) < 0.01)
                            {
                                matrix.postScale(-0.5f, 0.5f);
                            }
                            else if (Math.abs(player.zPos - roadLowerDist) < 0.01)
                            {
                                matrix.postScale(0.5f, 0.5f);
                            }

                            proj.SetSpriteSheet(Bitmap.createBitmap(playerProjBoomerangbmp, 0, 0, playerProjBoomerangbmp.getWidth(), playerProjBoomerangbmp.getHeight(), matrix, true), 2, 2, 24);

                            proj.xPos = player.xPos;
                            proj.yPos = player.yPos;
                            proj.zPos = player.zPos;
                            if (Math.abs(player.zPos - roadUpperDist) < 0.01)
                            {
                                proj.XSpeed = -1500;
                                proj.XAccel = 2000;
                                proj.ZAccel = 450;
                            }
                            else if (Math.abs(player.zPos - roadLowerDist) < 0.01)
                            {
                                proj.XSpeed = -1500;
                                proj.XAccel = 2000;
                                proj.ZAccel = -450;
                            }
                            proj.zLength = 30.0f;
                            proj.existTime = 2.0;
                            proj.type = SampleEntity.Type.Projectile;
                            proj.projtype = SampleEntity.ProjType.PlayerProj_Boomerang;
                            matrix.postScale(-2, 2);

                            player.state = SampleEntity.State.Attacking;
                            player.stateTime = 0.5;

                        }
                        else if (player.Weapon == SampleEntity.ProjType.PlayerProj_JumpingJack)
                        {
                            Matrix matrix = new Matrix();
                            SampleEntity proj = SampleEntity.Create();
                            matrix.postScale(-0.5f, 0.5f);
                            //playerProjJJbmp = Bitmap.createBitmap(playerproj.mesh);

                            proj.SetSpriteSheet(Bitmap.createBitmap(playerProjJJbmp, 0, 0, playerProjJJbmp.getWidth(), playerProjJJbmp.getHeight(), matrix, true), 1, 1, 12);

                            proj.xPos = player.xPos;
                            proj.yPos = player.yPos;
                            proj.zPos = player.zPos;
                            proj.XSpeed = -1000;
                            proj.YSpeed = -400;
                            proj.YAccel = 800;
                            proj.existTime = 3.0;
                            proj.type = SampleEntity.Type.Projectile;
                            proj.projtype = SampleEntity.ProjType.PlayerProj_JumpingJack;
                            matrix.postScale(-2, 2);

                            player.state = SampleEntity.State.Attacking;
                            player.stateTime = 0.5;
                        }
                    }

                    return true;
                }
                hitbox = roadTouchRight.getBounds();
                if (Collision.AABBCollision(0, playerRoadTouchboxOffset, 0, m.getX(), m.getY(), 0,
                        hitbox.left, hitbox.top, -0.2f, hitbox.right, hitbox.bottom, 0.2f, -0, -0, -0, 0, 0, 0)) {
                    if (player.state == SampleEntity.State.Normal) {
                        if (player.Weapon == SampleEntity.ProjType.PlayerProj_FutureGhost) {
                            Matrix matrix = new Matrix();
                            SampleEntity proj = SampleEntity.Create();
                            matrix.postScale(0.5f, 0.5f);
                            playerProjGhostbmp = Bitmap.createBitmap(playerproj.mesh);

                            proj.SetSpriteSheet(Bitmap.createBitmap(playerProjGhostbmp, 0, 0, playerProjGhostbmp.getWidth(), playerProjGhostbmp.getHeight(), matrix, true), 1, 1, 12);

                            proj.xPos = player.xPos;
                            proj.yPos = player.yPos;
                            proj.zPos = player.zPos;
                            proj.XSpeed = 1300;
                            proj.XAccel = -1300;
                            proj.existTime = 1.0;
                            proj.type = SampleEntity.Type.Projectile;
                            proj.projtype = SampleEntity.ProjType.PlayerProj_FutureGhost;
                            matrix.postScale(2, 2);

                            player.state = SampleEntity.State.Attacking;
                            player.stateTime = 0.5;
                        }
                        else if (player.Weapon == SampleEntity.ProjType.PlayerProj_Boomerang)
                        {
                            Matrix matrix = new Matrix();
                            SampleEntity proj = SampleEntity.Create();
                            if (Math.abs(player.zPos - roadUpperDist) < 0.01)
                            {
                                matrix.postScale(0.5f, 0.5f);
                            }
                            else if (Math.abs(player.zPos - roadLowerDist) < 0.01)
                            {
                                matrix.postScale(-0.5f, 0.5f);
                            }

                            proj.SetSpriteSheet(Bitmap.createBitmap(playerProjBoomerangbmp, 0, 0, playerProjBoomerangbmp.getWidth(), playerProjBoomerangbmp.getHeight(), matrix, true), 2, 2, 24);

                            proj.xPos = player.xPos;
                            proj.yPos = player.yPos;
                            proj.zPos = player.zPos;
                            if (Math.abs(player.zPos - roadUpperDist) < 0.01)
                            {
                                proj.XSpeed = 1500;
                                proj.XAccel = -2000;
                                proj.ZAccel = 450;
                            }
                            else if (Math.abs(player.zPos - roadLowerDist) < 0.01)
                            {
                                proj.XSpeed = 1500;
                                proj.XAccel = -2000;
                                proj.ZAccel = -450;
                            }
                            proj.zLength = 30.0f;
                            proj.existTime = 2.0;
                            proj.type = SampleEntity.Type.Projectile;
                            proj.projtype = SampleEntity.ProjType.PlayerProj_Boomerang;
                            matrix.postScale(-2, 2);

                            player.state = SampleEntity.State.Attacking;
                            player.stateTime = 0.5;
                        }
                        else if (player.Weapon == SampleEntity.ProjType.PlayerProj_JumpingJack)
                        {
                            Matrix matrix = new Matrix();
                            SampleEntity proj = SampleEntity.Create();
                            matrix.postScale(0.5f, 0.5f);
                            //playerProjJJbmp = Bitmap.createBitmap(playerproj.mesh);

                            proj.SetSpriteSheet(Bitmap.createBitmap(playerProjJJbmp, 0, 0, playerProjJJbmp.getWidth(), playerProjJJbmp.getHeight(), matrix, true), 1, 1, 12);

                            proj.xPos = player.xPos;
                            proj.yPos = player.yPos;
                            proj.zPos = player.zPos;
                            proj.XSpeed = 1000;
                            proj.YSpeed = -400;
                            proj.YAccel = 800;
                            proj.existTime = 3.0;
                            proj.type = SampleEntity.Type.Projectile;
                            proj.projtype = SampleEntity.ProjType.PlayerProj_JumpingJack;
                            matrix.postScale(-2, 2);

                            player.state = SampleEntity.State.Attacking;
                            player.stateTime = 0.5;
                        }
                    }
                    return true;
                }
            }
        }
        if (TouchManager.Instance.dir != TouchManager.SwipeDirection.NONE && swipeControls) {

            //System.out.println(hitbox.left + " | " + hitbox.top + " | " + hitbox.right + " | " + hitbox.bottom);
            if ((TouchManager.Instance.dir == TouchManager.SwipeDirection.UP && Math.abs(player.zPos - roadLowerDist) < 0.01) || (TouchManager.Instance.dir == TouchManager.SwipeDirection.DOWN && Math.abs(player.zPos - roadUpperDist) < 0.01)) {
                if (player.state == SampleEntity.State.Normal) {
                    if (Math.abs(player.zPos - roadUpperDist) < 0.01) {
                        player.state = SampleEntity.State.Jumping;
                        player.YSpeed = -400; //does not change depending where
                        player.ZSpeed = 400;//changes depending where
                        player.YAccel = 1600;
                        player.stateTime = 0.5;
                    } else if (Math.abs(player.zPos - roadLowerDist) < 0.01) {
                        player.state = SampleEntity.State.Jumping;
                        player.YSpeed = -400; //does not change depending where
                        player.ZSpeed = -400;//changes depending where
                        player.YAccel = 1600;
                        player.stateTime = 0.5;
                    }
                }
                return true;
            }

            if (TouchManager.Instance.dir == TouchManager.SwipeDirection.LEFT) {
                if (player.state == SampleEntity.State.Normal) {
                    if (player.Weapon == SampleEntity.ProjType.PlayerProj_FutureGhost) {
                        Matrix matrix = new Matrix();
                        SampleEntity proj = SampleEntity.Create();
                        matrix.postScale(-0.5f, 0.5f);
                        playerProjGhostbmp = Bitmap.createBitmap(playerproj.mesh);

                        proj.SetSpriteSheet(Bitmap.createBitmap(playerProjGhostbmp, 0, 0, playerProjGhostbmp.getWidth(), playerProjGhostbmp.getHeight(), matrix, true), 1, 1, 12);

                        proj.xPos = player.xPos;
                        proj.yPos = player.yPos;
                        proj.zPos = player.zPos;
                        proj.XSpeed = -1300;
                        proj.XAccel = 1300;
                        proj.existTime = 1.0;
                        proj.type = SampleEntity.Type.Projectile;
                        proj.projtype = SampleEntity.ProjType.PlayerProj_FutureGhost;
                        matrix.postScale(-2, 2);

                        player.state = SampleEntity.State.Attacking;
                        player.stateTime = 0.5;
                    } else if (player.Weapon == SampleEntity.ProjType.PlayerProj_Boomerang) {
                        Matrix matrix = new Matrix();
                        SampleEntity proj = SampleEntity.Create();
                        if (Math.abs(player.zPos - roadUpperDist) < 0.01) {
                            matrix.postScale(-0.5f, 0.5f);
                        } else if (Math.abs(player.zPos - roadLowerDist) < 0.01) {
                            matrix.postScale(0.5f, 0.5f);
                        }

                        proj.SetSpriteSheet(Bitmap.createBitmap(playerProjBoomerangbmp, 0, 0, playerProjBoomerangbmp.getWidth(), playerProjBoomerangbmp.getHeight(), matrix, true), 2, 2, 24);

                        proj.xPos = player.xPos;
                        proj.yPos = player.yPos;
                        proj.zPos = player.zPos;
                        if (Math.abs(player.zPos - roadUpperDist) < 0.01) {
                            proj.XSpeed = -1500;
                            proj.XAccel = 2000;
                            proj.ZAccel = 450;
                        } else if (Math.abs(player.zPos - roadLowerDist) < 0.01) {
                            proj.XSpeed = -1500;
                            proj.XAccel = 2000;
                            proj.ZAccel = -450;
                        }
                        proj.zLength = 30.0f;
                        proj.existTime = 2.0;
                        proj.type = SampleEntity.Type.Projectile;
                        proj.projtype = SampleEntity.ProjType.PlayerProj_Boomerang;
                        matrix.postScale(-2, 2);

                        player.state = SampleEntity.State.Attacking;
                        player.stateTime = 0.5;
                    }
                    else if (player.Weapon == SampleEntity.ProjType.PlayerProj_JumpingJack)
                    {
                        Matrix matrix = new Matrix();
                        SampleEntity proj = SampleEntity.Create();
                        matrix.postScale(-0.5f, 0.5f);
                        //playerProjJJbmp = Bitmap.createBitmap(playerproj.mesh);

                        proj.SetSpriteSheet(Bitmap.createBitmap(playerProjJJbmp, 0, 0, playerProjJJbmp.getWidth(), playerProjJJbmp.getHeight(), matrix, true), 1, 1, 12);

                        proj.xPos = player.xPos;
                        proj.yPos = player.yPos;
                        proj.zPos = player.zPos;
                        proj.XSpeed = -1000;
                        proj.YSpeed = -400;
                        proj.YAccel = 800;
                        proj.existTime = 3.0;
                        proj.type = SampleEntity.Type.Projectile;
                        proj.projtype = SampleEntity.ProjType.PlayerProj_JumpingJack;
                        matrix.postScale(-2, 2);

                        player.state = SampleEntity.State.Attacking;
                        player.stateTime = 0.5;
                    }
                }

                return true;
            }

            if (TouchManager.Instance.dir == TouchManager.SwipeDirection.RIGHT) {
                if (player.state == SampleEntity.State.Normal) {
                    if (player.Weapon == SampleEntity.ProjType.PlayerProj_FutureGhost) {
                        Matrix matrix = new Matrix();
                        SampleEntity proj = SampleEntity.Create();
                        matrix.postScale(0.5f, 0.5f);
                        playerProjGhostbmp = Bitmap.createBitmap(playerproj.mesh);

                        proj.SetSpriteSheet(Bitmap.createBitmap(playerProjGhostbmp, 0, 0, playerProjGhostbmp.getWidth(), playerProjGhostbmp.getHeight(), matrix, true), 1, 1, 12);

                        proj.xPos = player.xPos;
                        proj.yPos = player.yPos;
                        proj.zPos = player.zPos;
                        proj.XSpeed = 1300;
                        proj.XAccel = -1300;
                        proj.existTime = 1.0;
                        proj.type = SampleEntity.Type.Projectile;
                        proj.projtype = SampleEntity.ProjType.PlayerProj_FutureGhost;
                        matrix.postScale(2, 2);

                        player.state = SampleEntity.State.Attacking;
                        player.stateTime = 0.5;
                    } else if (player.Weapon == SampleEntity.ProjType.PlayerProj_Boomerang) {
                        Matrix matrix = new Matrix();
                        SampleEntity proj = SampleEntity.Create();
                        if (Math.abs(player.zPos - roadUpperDist) < 0.01) {
                            matrix.postScale(0.5f, 0.5f);
                        } else if (Math.abs(player.zPos - roadLowerDist) < 0.01) {
                            matrix.postScale(-0.5f, 0.5f);
                        }

                        proj.SetSpriteSheet(Bitmap.createBitmap(playerProjBoomerangbmp, 0, 0, playerProjBoomerangbmp.getWidth(), playerProjBoomerangbmp.getHeight(), matrix, true), 2, 2, 24);

                        proj.xPos = player.xPos;
                        proj.yPos = player.yPos;
                        proj.zPos = player.zPos;
                        if (Math.abs(player.zPos - roadUpperDist) < 0.01) {
                            proj.XSpeed = 1500;
                            proj.XAccel = -2000;
                            proj.ZAccel = 450;
                        } else if (Math.abs(player.zPos - roadLowerDist) < 0.01) {
                            proj.XSpeed = 1500;
                            proj.XAccel = -2000;
                            proj.ZAccel = -450;
                        }
                        proj.zLength = 30.0f;
                        proj.existTime = 2.0;
                        proj.type = SampleEntity.Type.Projectile;
                        proj.projtype = SampleEntity.ProjType.PlayerProj_Boomerang;
                        matrix.postScale(-2, 2);

                        player.state = SampleEntity.State.Attacking;
                        player.stateTime = 0.5;
                    }
                    else if (player.Weapon == SampleEntity.ProjType.PlayerProj_JumpingJack)
                    {
                        Matrix matrix = new Matrix();
                        SampleEntity proj = SampleEntity.Create();
                        matrix.postScale(0.5f, 0.5f);
                        //playerProjJJbmp = Bitmap.createBitmap(playerproj.mesh);

                        proj.SetSpriteSheet(Bitmap.createBitmap(playerProjJJbmp, 0, 0, playerProjJJbmp.getWidth(), playerProjJJbmp.getHeight(), matrix, true), 1, 1, 12);

                        proj.xPos = player.xPos;
                        proj.yPos = player.yPos;
                        proj.zPos = player.zPos;
                        proj.XSpeed = 1000;
                        proj.YSpeed = -400;
                        proj.YAccel = 800;
                        proj.existTime = 3.0;
                        proj.type = SampleEntity.Type.Projectile;
                        proj.projtype = SampleEntity.ProjType.PlayerProj_JumpingJack;
                        matrix.postScale(-2, 2);

                        player.state = SampleEntity.State.Attacking;
                        player.stateTime = 0.5;
                    }
                }
                return true;
            }
        }

        }
        else if (gamePaused)
        {
            if (!InSettings)
            {
                if(TouchManager.Instance.HasTapped()) {
                    hitbox = pause_SettingsRegion.getBounds();
                    if (Collision.AABBCollision(0, 0, 0, m.getX(), m.getY(), 0,
                            hitbox.left, hitbox.top, -0.2f, hitbox.right, hitbox.bottom, 0.2f, -0, -0, -0, 0, 0, 0)) {

                        InSettings = true;

                        return true;
                    }
                    hitbox = pause_ExitRegion.getBounds();
                    if (Collision.AABBCollision(0, 0, 0, m.getX(), m.getY(), 0,
                            hitbox.left, hitbox.top, -0.2f, hitbox.right, hitbox.bottom, 0.2f, -0, -0, -0, 0, 0, 0)) {

                        //Exit to main menu (NOT IMPLEMENTED YET, RMB CLEAR ALL THAT)

                        //Take note that alerts DO NOT halt the game
                        AlertDialog.Builder builder;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            builder = new AlertDialog.Builder(MusicController.Instance.gamepageContext, android.R.style.Theme_Material_Dialog_Alert);
                        } else {
                            builder = new AlertDialog.Builder(MusicController.Instance.gamepageContext);
                        }
                        builder.setTitle("Exit Button")
                                .setMessage("Are you sure you want to go back to the main menu?")
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int a) {
                                        // results of yes!
                                        Intent intent = new Intent(); //Intent = action to be performed
                                        intent.setClass(MusicController.Instance.gamepageContext, Mainmenu.class);

                                        finish();
                                        MusicController.Instance.gamepageContext.startActivity(intent);
                                        GameRunning = false;
                                        //EntityManager.Instance.DeleteAllThat();
                                        EntityManager.Instance.DoneAllThat();
                                        MusicController.Instance.Stop();
                                    }
                                })
                                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int a) {
                                        // results of no!
                                    }
                                })
                                .setIcon(android.R.drawable.ic_dialog_alert)
                                .show();

                        return true;
                    }
                }
            }
            else if (InSettings)
            {
                hitbox = settings_MusicSlider.getBounds();
                hitbox.left += (int)((MusicController.Instance.MusicVolume * 0.01) * 0.5 * screenWidth);
                hitbox.right += (int)((MusicController.Instance.MusicVolume * 0.01) * 0.5 * screenWidth);
                if (Collision.AABBCollision(0, 0, 0, m.getX(), m.getY(), 0,
                        hitbox.left, hitbox.top, -0.2f, hitbox.right, hitbox.bottom, 0.2f, -0, -0, -0, 0, 0, 0)) {

                    if (TouchManager.Instance.held)
                    {
                        MusicController.Instance.MusicVolume += TouchManager.Instance.heldDownX / (0.01 * 0.5 * screenWidth);
                        if (MusicController.Instance.MusicVolume < 0 )MusicController.Instance.MusicVolume = 0;
                        if (MusicController.Instance.MusicVolume > 100)MusicController.Instance.MusicVolume = 100;
                    }

                    return true;
                }

                hitbox = settings_SoundSlider.getBounds();
                hitbox.left += (int)((MusicController.Instance.SoundVolume * 0.01) * 0.5 * screenWidth);
                hitbox.right += (int)((MusicController.Instance.SoundVolume * 0.01) * 0.5 * screenWidth);
                if (Collision.AABBCollision(0, 0, 0, m.getX(), m.getY(), 0,
                        hitbox.left, hitbox.top, -0.2f, hitbox.right, hitbox.bottom, 0.2f, -0, -0, -0, 0, 0, 0)) {

                    if (TouchManager.Instance.held)
                    {
                        MusicController.Instance.SoundVolume += TouchManager.Instance.heldDownX / (0.01 * 0.5 * screenWidth);
                        if (MusicController.Instance.SoundVolume < 0 )MusicController.Instance.SoundVolume = 0;
                        if (MusicController.Instance.SoundVolume > 100)MusicController.Instance.SoundVolume = 100;
                    }

                    return true;
                }

                if(TouchManager.Instance.HasTapped()) {
                    hitbox = settings_SkipDialogueToggleRegion.getBounds();
                    if (Collision.AABBCollision(0, 0, 0, m.getX(), m.getY(), 0,
                            hitbox.left, hitbox.top, -0.2f, hitbox.right, hitbox.bottom, 0.2f, -0, -0, -0, 0, 0, 0)) {

                        skipDialogue = !skipDialogue;

                        return true;
                    }
                    hitbox = settings_Back.getBounds();
                    if (Collision.AABBCollision(0, 0, 0, m.getX(), m.getY(), 0,
                            hitbox.left, hitbox.top, -0.2f, hitbox.right, hitbox.bottom, 0.2f, -0, -0, -0, 0, 0, 0)) {

                        InSettings = false;

                        return true;
                    }
                }
            }


        }


        return true;
    }


    public Boolean LevelPacketCondition(Level.Condition condition)
    {
        if (condition == Level.Condition.NO_ENEMY)
        {
            if (!EntityManager.Instance.EnemyPresent() && enemyqueue.size() == 0)
            {
                return true;
            }
            return false;
        }
        else if (condition == Level.Condition.TIME_PASSED)
        {
            if (CurrentDialogueRemainingTime <= 0)
            {
                return true;
            }
            return false;
        }

        return true;
    }

    public LinkedList<String> readLine(String path) {
        LinkedList<String> mLines = new LinkedList<String>();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(MusicController.Instance.gamepageContext.getAssets().open(path)));
            String line;

            while ((line = reader.readLine()) != null)
                ((LinkedList<String>) mLines).add(mLines.size(), line);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return mLines;
    }

    void StartLevel()
    {
        levelInfo.clear();

        LinkedList<String> textInfo = new LinkedList<>();
        textInfo = readLine(MusicController.Instance.leveltextfile);
        boolean readingenemypacket = false;
        LevelPacket EnemyPacketstandby = new LevelPacket(Level.DataType.NONE, Level.Condition.NONE);
        String levelpacketID = "", levelpacketcondition = "", thirdstring = "", fourthstring = "";
        int stagepack = 0;
        for (String currentline : textInfo)
        {
            levelpacketID = "";
            levelpacketcondition = "";
            thirdstring = "";
            fourthstring = "";
            stagepack = 0;
            //System.out.println(currentline);

            if (readingenemypacket)
            {
                String[] TempEnemyStatStorage = new String[6];
                TempEnemyStatStorage[0] = "";
                TempEnemyStatStorage[1] = "";
                TempEnemyStatStorage[2] = "";
                TempEnemyStatStorage[3] = "";
                TempEnemyStatStorage[4] = "";
                TempEnemyStatStorage[5] = "";
                boolean stoppedenemyread = false;
                for (int i = 0; i < currentline.length(); ++i) {
                    //System.out.println(TempEnemyStatStorage[stagepack]);
                    if (currentline.charAt(i) != '|') {
                        TempEnemyStatStorage[stagepack] += currentline.charAt(i);
                    }
                    else
                    {
                        {
                            stagepack++;
                        }
                    }
                }

                if (TempEnemyStatStorage[stagepack].compareTo("StopEnemyRead") == 0)
                {
                    levelInfo.add(EnemyPacketstandby);
                    stoppedenemyread = true;
                    readingenemypacket = false;
                }


                if (!stoppedenemyread) {
                    EnemyData tempenemydata = new EnemyData(SampleEntity.Type.FossilCar, 0, EnemyData.SpawnPoint.BOTTOMRIGHT, false, 1, 1);

                    if (TempEnemyStatStorage[0].compareTo("FossilCar") == 0) {
                        tempenemydata.enemytype = SampleEntity.Type.FossilCar;
                    }
                    tempenemydata.timeToSpawn = Double.parseDouble(TempEnemyStatStorage[1]);
                    if (TempEnemyStatStorage[2].compareTo("TopLeft") == 0) {
                        tempenemydata.spawn = EnemyData.SpawnPoint.TOPLEFT;
                    } else if (TempEnemyStatStorage[2].compareTo("BottomLeft") == 0) {
                        tempenemydata.spawn = EnemyData.SpawnPoint.BOTTOMLEFT;
                    } else if (TempEnemyStatStorage[2].compareTo("TopRight") == 0) {
                        tempenemydata.spawn = EnemyData.SpawnPoint.TOPRIGHT;
                    } else if (TempEnemyStatStorage[2].compareTo("BottomRight") == 0) {
                        tempenemydata.spawn = EnemyData.SpawnPoint.BOTTOMRIGHT;
                    }

                    if (TempEnemyStatStorage[3].compareTo("true") == 0) {
                        tempenemydata.canSpecial = true;
                    } else if (TempEnemyStatStorage[3].compareTo("false") == 0) {
                        tempenemydata.canSpecial = false;
                    }

                    tempenemydata.hp = Float.parseFloat(TempEnemyStatStorage[4]);
                    tempenemydata.damage = Float.parseFloat(TempEnemyStatStorage[5]);

                    EnemyPacketstandby.enemylist.add(tempenemydata);

                }
            }
            else {
                for (int i = 0; i < currentline.length(); ++i) {
                    if (currentline.charAt(i) != '|') {
                        if (stagepack == 0) {
                            levelpacketID += currentline.charAt(i);
                        } else if (stagepack == 1) {
                            levelpacketcondition += currentline.charAt(i);
                        } else if (stagepack == 2) {
                            thirdstring += currentline.charAt(i);
                        } else if (stagepack == 3) {
                            fourthstring += currentline.charAt(i);
                        }
                    } else {
                        stagepack++;

                    }
                }

                if (levelpacketID.compareTo("") != 0) {
                    Level.DataType tempDatatype = Level.DataType.NONE;
                    Level.Condition tempCondition = Level.Condition.NONE;

                    if (levelpacketID.compareTo("Dialogue") == 0) {
                        tempDatatype = Level.DataType.DIALOGUE;
                    } else if (levelpacketID.compareTo("Music") == 0) {
                        tempDatatype = Level.DataType.MUSIC;
                    } else if (levelpacketID.compareTo("WinGame") == 0) {
                        tempDatatype = Level.DataType.WINGAME;
                    } else if (levelpacketID.compareTo("Enemy") == 0) {
                        tempDatatype = Level.DataType.ENEMY;
                    } else if (levelpacketID.compareTo("Startlevel") == 0)
                    {
                        tempDatatype = Level.DataType.NONE;

                        if (levelpacketcondition.compareTo("Ruinedcity") == 0)
                        {
                            sea.mesh = rc_wasteland;
                            rock0.mesh = rc_smoke0;
                            rock1.mesh = rc_smoke1;
                        }
                        else if (levelpacketcondition.compareTo("Blastedworld") == 0)
                        {
                            //predefined
                        }

                    }

                    if (tempDatatype == Level.DataType.ENEMY) {
                        if (levelpacketcondition.compareTo("None") == 0) {
                            tempCondition = Level.Condition.NONE;
                        } else if (levelpacketcondition.compareTo("TimePassed") == 0) {
                            tempCondition = Level.Condition.TIME_PASSED;
                        } else if (levelpacketcondition.compareTo("NoEnemy") == 0) {
                            tempCondition = Level.Condition.NO_ENEMY;
                        }

                        EnemyPacketstandby = new LevelPacket(tempDatatype, tempCondition);
                        readingenemypacket = true;

                    } else if (tempDatatype != Level.DataType.NONE) {

                        if (levelpacketcondition.compareTo("None") == 0) {
                            tempCondition = Level.Condition.NONE;
                        } else if (levelpacketcondition.compareTo("TimePassed") == 0) {
                            tempCondition = Level.Condition.TIME_PASSED;
                        } else if (levelpacketcondition.compareTo("NoEnemy") == 0) {
                            tempCondition = Level.Condition.NO_ENEMY;
                        }

                        LevelPacket pkt = new LevelPacket(tempDatatype, tempCondition);

                        if (levelpacketID.compareTo("Dialogue") == 0) {
                            pkt.dialogue = thirdstring;
                            pkt.duration = Double.parseDouble(fourthstring);
                            levelInfo.add(pkt);
                        }
                        else if (levelpacketID.compareTo("Music") == 0) {
                            if (thirdstring.compareTo("reborn") == 0)
                                pkt.musictrack = R.raw.reborn;
                            else if (thirdstring.compareTo("centurion") == 0)
                                pkt.musictrack = R.raw.centurion;
                            else if (thirdstring.compareTo("neptune") == 0)
                                pkt.musictrack = R.raw.neptune;
                            levelInfo.add(pkt);
                        }
                        else if (levelpacketID.compareTo("WinGame") == 0) {
                            pkt.dialogue = thirdstring;
                            levelInfo.add(pkt);
                        }
                    }
                }
            }
        }
        textInfo.clear();
    }


    @Override
    public void onBackPressed(){
        MusicController.Instance.Stop();
    }

    void FeedbackStart(int _x, int _y)
    {
        feedbackstart = true;
        feedbackX = _x;
        feedbackY = _y;
    }


    public void Update_CursorIndicator(double dt)
    {
        if (gameCountdownToEnd > 0.0 && gameresult != GAME_RESULT.GAME_ONGOING)
        {
            gameCountdownToEnd -= dt;

            if (finishscreen_offset < 0)
                finishscreen_offset += screenWidth * dt;
            if (finishscreen_offset > 0)
                finishscreen_offset = 0;

            if (gameCountdownToEnd <= 0.0)
            {
                Intent intent = new Intent();
                intent.setClass(MusicController.Instance.gamepageContext, Mainmenu.class);
                finish();
                MusicController.Instance.gamepageContext.startActivity(intent);
                GameRunning = false;
                EntityManager.Instance.DoneAllThat();
                MusicController.Instance.Stop();
            }
        }

        if (feedbackstart == false) return;

        if (feedback_lifetime > 0.0)
        {
            feedback_lifetime -= dt;

            if (feedback_lifetime <= 0.0)
            {
                feedbackstart = false;
                feedback_lifetime = 1.0;
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
// Do something here if sensor accuracy changes.
    }
    @Override
    public void onSensorChanged(SensorEvent SenseEvent) {
// Many sensors return 3 values, one for each axis.
// Do something with this sensor value.
        values = SenseEvent.values;
    }

    public void SensorMove() {
// Temp Variables
        float tempX, tempY;
// bX and bY are variables used for moving the object
// values [1] – sensor values for x axis
// values [0] – sensor values for y axis


        tempX = bX + (values[1] * ((System.currentTimeMillis() - lastTime) / 1000));
        tempY = bY + (values[0] * ((System.currentTimeMillis() - lastTime) / 1000));

        // Check if the ball is going out of screen along the x-axis
        if (tempX <= ball.getWidth()/2 || tempX >= screenWidth - ball.getWidth()/2)
        {
// Check if ball is still within screen along the y-axis
            if ( tempY > ball.getHeight()/2 && tempY < screenHeight - ball.getHeight()/2) {
                bY = tempY;
            }
        }
        // Check if the ball is going out of screen along the y-axis
        else if (tempY <= ball.getHeight()/2 || tempY >= screenHeight-ball.getHeight()/2)
        {
// Check if ball is still within screen along the x-axis
            if (tempX > ball.getWidth()/2 && tempX < screenWidth - ball.getWidth()/2) {
                bX = tempX;
            }
        }
        // If not, both axis of the ball's position is still within screen
        else
        {
// Move the ball with frame independantmovement
            bX = tempX;
            bY = tempY;
        }
    }

}
