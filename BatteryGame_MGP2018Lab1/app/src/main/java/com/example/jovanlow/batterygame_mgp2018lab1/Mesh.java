package com.example.jovanlow.batterygame_mgp2018lab1;

import android.graphics.Bitmap;

public class Mesh {
    Bitmap mesh; //pos
    float x = 0, y = 0, z = 0; //pos. Z coordinate is required.
    float minX = -0.5f, minY = -0.5f, minZ = -0.5f, maxX = 0.5f, maxY = 0.5f, maxZ = 0.5f; //aabb
    float hp = 100.0f;
    float velX = 0.0f, velY = 0.0f, velZ = 0.0f; //velocity
    float accelX = 0.0f, accelY = 0.0f, accelZ = 0.0f;
    boolean active = true;
    double bouncetime = 0.0f;
}
